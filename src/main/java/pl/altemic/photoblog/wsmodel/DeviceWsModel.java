package pl.altemic.photoblog.wsmodel;

import pl.altemic.photoblog.model.User;

/**
 * Created by michal on 02.09.2016.
 */
public class DeviceWsModel {
    public long id;
    public String name;
    public User user;

    public DeviceWsModel(){
     user = new User();
    }
}
