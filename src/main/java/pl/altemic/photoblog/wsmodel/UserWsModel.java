package pl.altemic.photoblog.wsmodel;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by michal on 02.09.2016.
 */
public class UserWsModel {
    public long id;
    public String firstname;
    public String lastname;
    public String username;
    public String email;
    public List<DeviceWsModel> devices;
    public List<RoleWsModel> roles;

    public UserWsModel(){
        devices = new ArrayList<>();
        roles = new ArrayList<>();
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public List<DeviceWsModel> getDevices() {
        return devices;
    }

    public void setDevices(List<DeviceWsModel> devices) {
        this.devices = devices;
    }

    public List<RoleWsModel> getRoles() {
        return roles;
    }

    public void setRoles(List<RoleWsModel> roles) {
        this.roles = roles;
    }
}
