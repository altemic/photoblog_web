package pl.altemic.photoblog.wsmodel;

import pl.altemic.photoblog.model.Picture;
import pl.altemic.photoblog.model.User;

import java.sql.Timestamp;

/**
 * Created by michal on 02.09.2016.
 */
public class RecordWsModel {
    private long id;
    private Picture picture;
    private String title;
    private String description;
    private User user;
    private Timestamp date;
}
