package pl.altemic.photoblog.wsmodel;

/**
 * Created by michal on 02.09.2016.
 */
public class RoleWsModel {
    public long id;
    public int accessLevel;
    public String title;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public int getAccessLevel() {
        return accessLevel;
    }

    public void setAccessLevel(int accessLevel) {
        this.accessLevel = accessLevel;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
