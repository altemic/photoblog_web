package pl.altemic.photoblog.wsmodel;

import java.sql.Timestamp;

/**
 * Created by michal on 02.09.2016.
 */
public class ChangeInfoWsModel {
    public long id;
    public String description;
    public String tableName;
    public Timestamp date;
    public long source_id;
}
