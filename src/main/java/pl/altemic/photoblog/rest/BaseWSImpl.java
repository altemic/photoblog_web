package pl.altemic.photoblog.rest;

import pl.altemic.photoblog.service.BaseCRUDService;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

/**
 * Created by michal on 02.09.2016.
 */
public abstract class BaseWSImpl<T, V> implements BaseWS<T> {

    @Inject
    protected BaseCRUDService<V> baseCRUDService;

    public abstract T FromDtoToWsModel(V model);
    public abstract V FromWsModelToDto(T wsModel);

    @POST
    @Path("/save")
    @Consumes(MediaType.APPLICATION_JSON)
    public void save(T obj){
        V model = FromWsModelToDto(obj);
        baseCRUDService.save(model);
    }

    @POST
    @Path("/remove/{id}")
    public void remove(@PathParam("id") Long id){
        baseCRUDService.remove(id);
    }

    @POST
    @Path("/update")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public T update(T obj){
        V model = FromWsModelToDto(obj);
        return FromDtoToWsModel(baseCRUDService.update(model));
    }

    @GET
    @Path("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public T get(@PathParam("id") Long id){
        return FromDtoToWsModel(baseCRUDService.get(id));
    }
}
