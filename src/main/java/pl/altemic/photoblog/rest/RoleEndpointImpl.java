package pl.altemic.photoblog.rest;

import pl.altemic.photoblog.dto.RoleDto;
import pl.altemic.photoblog.wsmodel.RoleWsModel;

import javax.ws.rs.Path;

/**
 * Created by michal on 02.09.2016.
 */
@Path("/roles")
public class RoleEndpointImpl extends BaseWSImpl<RoleWsModel, RoleDto> implements BaseWS<RoleWsModel> {

    @Override
    public RoleWsModel FromDtoToWsModel(RoleDto roleDto) {
        RoleWsModel roleWsModel = new RoleWsModel();
        roleWsModel.setId(roleDto.getId());
        roleWsModel.setAccessLevel(roleDto.getAccessLevel());
        roleWsModel.setTitle(roleDto.getTitle());
        return  roleWsModel;
    }

    @Override
    public RoleDto FromWsModelToDto(RoleWsModel wsModel) {
        RoleDto role = new RoleDto();
        role.setId(wsModel.getId());
        role.setAccessLevel(wsModel.getAccessLevel());
        role.setTitle(wsModel.getTitle());
        return role;
    }
}
