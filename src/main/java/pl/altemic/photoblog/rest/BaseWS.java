package pl.altemic.photoblog.rest;

/**
 * Created by michal on 02.09.2016.
 */
public interface BaseWS<T> {
    void save(T obj);
    void remove(Long id);
    T update(T obj);
    T get(Long id);
}
