package pl.altemic.photoblog.rest;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

/**
 * Created by michal on 27.08.2016.
 */
@ApplicationPath("/api")
public class RestConfiguration extends Application {
}
