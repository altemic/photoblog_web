package pl.altemic.photoblog.rest;

import pl.altemic.photoblog.dto.RoleDto;
import pl.altemic.photoblog.dto.UserDto;
import pl.altemic.photoblog.service.UserService;
import pl.altemic.photoblog.wsmodel.RoleWsModel;
import pl.altemic.photoblog.wsmodel.UserWsModel;

import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by michal on 27.08.2016.
 */

@Path("/users")
public class UserEndpointImpl extends BaseWSImpl<UserWsModel, UserDto> implements BaseWS<UserWsModel>{

    @Inject
    private UserService userService;

    @Inject
    private BaseWSImpl<RoleWsModel, RoleDto> roleEndpoint;

    @GET
    @Path("/all")
    @Produces(MediaType.APPLICATION_JSON)
    public List<UserWsModel> findAll() {
        List<UserDto> userList = userService.findAll();
        List<UserWsModel> userWsModels = userList.stream().map(u -> FromDtoToWsModel(u)).collect(Collectors.toList());
        return userWsModels;
    }

    public UserWsModel FromDtoToWsModel(UserDto user){
        UserWsModel userWsModel = new UserWsModel();
        userWsModel.setId(user.getId());
        userWsModel.setUsername(user.getUsername());
        userWsModel.setFirstname(user.getFirstname());
        userWsModel.setLastname(user.getLastname());
        List<RoleWsModel> roleWsList = user.getRoles().stream().map(r -> roleEndpoint.FromDtoToWsModel(r)).collect(Collectors.toList());
        userWsModel.setRoles(roleWsList);
        return userWsModel;
    }

    public UserDto FromWsModelToDto(UserWsModel userWsModel){
        UserDto userDto = new UserDto();
        userDto.setId(userWsModel.getId());
        userDto.setFirstname(userWsModel.getFirstname());
        userDto.setLastname(userWsModel.getLastname());
        userDto.setUsername(userWsModel.getUsername());
        List<RoleDto> roleList = userWsModel.getRoles().stream().map(r -> roleEndpoint.FromWsModelToDto(r)).collect(Collectors.toList());
        userDto.setRoles(roleList);
        return userDto;
    }
}
