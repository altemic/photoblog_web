package pl.altemic.photoblog.dto;

/**
 * Created by michal on 02.09.2016.
 */
public class DeviceDto {
    public long id;
    public String name;
    public UserDto user;
    public DeviceDto(){
     user = new UserDto();
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public UserDto getUser() {
        return user;
    }

    public void setUser(UserDto user) {
        this.user = user;
    }
}
