package pl.altemic.photoblog.dto;

/**
 * Created by michal on 02.09.2016.
 */
public class PictureDto {
    public long id;
    public byte[] picture;

    public String getExtension() { return extension; }

    public void setExtension(String extension) { this.extension = extension; }

    private String extension;

    public RecordDto getRecord() {
        return record;
    }

    public void setRecord(RecordDto record) {
        this.record = record;
    }

    public RecordDto record;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public byte[] getPicture() {
        return picture;
    }

    public void setPicture(byte[] picture) {
        this.picture = picture;
    }
}
