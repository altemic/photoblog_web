package pl.altemic.photoblog.dto;

import java.sql.Timestamp;

/**
 * Created by michal on 02.09.2016.
 */
public class ChangeInfoDto {
    public long id;
    public String description;
    public String tableName;
    public Timestamp date;
    public long source_id;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getTableName() {
        return tableName;
    }

    public void setTableName(String tableName) {
        this.tableName = tableName;
    }

    public Timestamp getDate() {
        return date;
    }

    public void setDate(Timestamp date) {
        this.date = date;
    }

    public long getSource_id() {
        return source_id;
    }

    public void setSource_id(long source_id) {
        this.source_id = source_id;
    }
}
