package pl.altemic.photoblog.dto;

import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by michal on 18.09.2016.
 */
public class Mapper {
    public static <T, V> V Convert(T initial, V result) {
        try {
            Field[] initialObjectFieldsArray = initial.getClass().getDeclaredFields();
            Field[] resultObjectFieldsArray = result.getClass().getDeclaredFields();
            List<Field> resultObjectFieldsList = Arrays.asList(resultObjectFieldsArray);
            for (Field initialObjField : initialObjectFieldsArray) {
                initialObjField.setAccessible(true);
                String name = initialObjField.getName();
                Object value = initialObjField.get(initial);
                if (resultObjectFieldsList.stream().anyMatch(x -> x.getName().equals(name))) {
                    List<Field> resultObjFields = resultObjectFieldsList.stream().filter(x -> x.getName().equals(name)).collect(Collectors.toList());
                    Field f = resultObjFields.get(0);
                    f.setAccessible(true);
                    f.set(result, value);
                }
            }
        } catch (IllegalAccessException e){
            e.printStackTrace();
        }
        return result;
    }
}
