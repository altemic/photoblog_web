package pl.altemic.photoblog.viewmodel;

/**
 * Created by michal on 28.08.2016.
 */
public class PictureViewModel {
    private long id;
    private byte[] picture;
    private RecordViewModel record;
    private String extension;

    public void setId(long id) { this.id = id; }

    public String getExtension() { return extension; }

    public void setExtension(String extension) { this.extension = extension; }

    public PictureViewModel() {}

    public long getId() {
        return id;
    }

    public void setId(Long id) { this.id = id; }

    public byte[] getPicture() {
        return picture;
    }

    public void setPicture(byte[] picture) {
        this.picture = picture;
    }

    public RecordViewModel getRecord() { return record; }

    public void setRecord(RecordViewModel record) { this.record = record; }
}
