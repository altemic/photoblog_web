package pl.altemic.photoblog.viewmodel;

/**
 * Created by michal on 28.08.2016.
 */
public class DeviceViewModel {
    private long id;
    private String name;
    private UserViewModel user;

    public DeviceViewModel() {}

    public void setName(String name) { this.name = name; }

    public void setUser(UserViewModel user) { this.user = user; }

    public long getId() { return id; }

    public String getName() { return name; }

    public UserViewModel getUser() { return user; }
}
