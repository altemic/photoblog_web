package pl.altemic.photoblog.viewmodel;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by michal on 26.08.2016.
 */
public class UserViewModel {
    private long id;
    private String firstname;
    private String lastname;
    private String username;
    private String email;
    private String password;
    private List<RoleViewModel> roles;
    private List<DeviceViewModel> devices;

    public UserViewModel() {
        roles = new ArrayList<>();
        devices = new ArrayList<>();
    }

    public String getRolesString(){
        List<String> rolesList
                = roles.stream().map(r -> r.getTitle()).collect(Collectors.toList());
        return String.join(", ", rolesList);
    }

    public String getFirstname() { return firstname; }

    public String getLastname() {
        return lastname;
    }

    public String getUsername() {
        return username;
    }

    public String getEmail() {
        return email;
    }

    public List<RoleViewModel> getRoles() {
        return roles;
    }

    public long getId() { return id; }

    public void setId(long id) { this.id = id; }

    public void setFirstname(String firstname) { this.firstname = firstname; }

    public void setLastname(String lastname) { this.lastname = lastname; }

    public void setUsername(String username) { this.username = username; }

    public void setEmail(String email) { this.email = email; }

    public void setRoles(List<RoleViewModel> roles) { this.roles = roles; }

    public List<DeviceViewModel> getDevices() { return devices; }

    public void setDevices(List<DeviceViewModel> devices) { this.devices = devices; }

    public String getPassword() { return password; }

    public void setPassword(String password) { this.password = password; }
}
