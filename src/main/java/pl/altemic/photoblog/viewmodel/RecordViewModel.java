package pl.altemic.photoblog.viewmodel;

import java.sql.Timestamp;

/**
 * Created by michal on 28.08.2016.
 */
public class RecordViewModel {
    private long id;
    private PictureViewModel picture;
    private String title;
    private String description;
    private Timestamp date;
    private UserViewModel user;

    public RecordViewModel() {}

    public long getId() {
        return id;
    }

    public PictureViewModel getPicture() {
        return picture;
    }

    public void setPicture(PictureViewModel picture) { this.picture = picture; }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setId(long id) { this.id = id; }

    public Timestamp getDate() { return date; }

    public void setDate(Timestamp date) { this.date = date; }

    public UserViewModel getUser() { return user; }

    public void setUser(UserViewModel user) { this.user = user; }

    public String getDateString(){
        return getDate().toString();
    }
}
