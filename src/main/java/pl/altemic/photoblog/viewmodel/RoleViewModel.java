package pl.altemic.photoblog.viewmodel;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by michal on 27.08.2016.
 */
public class RoleViewModel {
    private Long id;
    private int accessLevel;
    private String title;
    private List<UserViewModel> users;

    public RoleViewModel() {
        users = new ArrayList<>();
    }

    public void setId(Long id) { this.id = id; }

    public Long getId() {
        return id;
    }

    public int getAccessLevel() {
        return accessLevel;
    }

    public void setAccessLevel(int accessLevel) {
        this.accessLevel = accessLevel;
    }

    public String getTitle() { return title; }

    public void setTitle(String title) { this.title = title; }

    public List<UserViewModel> getUsers() { return users; }

    public void setUsers(List<UserViewModel> users) { this.users = users; }
}
