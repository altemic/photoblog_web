package pl.altemic.photoblog.filter;

import pl.altemic.photoblog.util.ConfigScheme;
import pl.altemic.photoblog.util.ConfigService;

import javax.inject.Inject;
import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.IOException;

/**
 * Created by michal on 16.10.2016.
 */
@WebFilter(filterName = "addConfigFilter", urlPatterns = "/*")
public class addConfigFilter implements Filter {

    @Inject
    private ConfigService configService;

    public void destroy() {
    }

    public void doFilter(ServletRequest req, ServletResponse resp, FilterChain chain) throws ServletException, IOException {
        HttpServletRequest request = (HttpServletRequest)req;
        HttpSession session = request.getSession(true);
        ConfigScheme cfgScheme = configService.load();
        session.setAttribute("Config", cfgScheme);
        chain.doFilter(req, resp);
    }

    public void init(FilterConfig config) throws ServletException {

    }

}
