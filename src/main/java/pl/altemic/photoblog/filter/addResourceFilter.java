package pl.altemic.photoblog.filter;

import pl.altemic.photoblog.service.ResourceService;

import javax.inject.Inject;
import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.IOException;

/**
 * Created by michal on 16.10.2016.
 */
@WebFilter(filterName = "addResourceFilter", urlPatterns = "/*")
public class addResourceFilter implements Filter {

    @Inject
    private ResourceService resourceService;

    public void destroy() {

    }

    public void doFilter(ServletRequest req, ServletResponse resp, FilterChain chain) throws ServletException, IOException {
        String languageParam = req.getParameter("lang");
        HttpServletRequest request = (HttpServletRequest)req;
        HttpSession session = request.getSession(true);
        if(languageParam != null){
            resourceService.setLanguage(languageParam);
        }
        session.setAttribute("resourceService", resourceService);
        chain.doFilter(req, resp);
    }

    public void init(FilterConfig config) throws ServletException {

    }
}
