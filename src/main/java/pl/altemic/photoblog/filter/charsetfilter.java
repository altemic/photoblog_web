package pl.altemic.photoblog.filter;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import java.io.IOException;

/**
 * Created by michal on 13.10.2016.
 */
@WebFilter(filterName = "CharsetFilter", urlPatterns = "/*")
public class charsetfilter implements Filter {
    private String encoding;
    public void destroy() {
    }

    public void doFilter(ServletRequest req, ServletResponse resp, FilterChain chain) throws ServletException, IOException {
        if (null == req.getCharacterEncoding()) {
            req.setCharacterEncoding(encoding);
            resp.setContentType("text/html; charset=UTF-8");
        }
        chain.doFilter(req, resp);
    }

    public void init(FilterConfig config) throws ServletException {
        encoding = config.getInitParameter("requestEncoding");
        if (encoding == null) encoding = "UTF-8";
    }

}
