package pl.altemic.photoblog.model;

import com.sun.istack.internal.NotNull;
import pl.altemic.photoblog.viewmodel.RoleViewModel;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by michal on 18.08.2016.
 */
@Entity(name = "role")
@NamedQueries({
        @NamedQuery(name="Role.findAll", query = "SELECT r FROM role r"),
        @NamedQuery(name="Role.findByName", query = "SELECT r FROM role r WHERE r.title = ?1")})
public class Role {
    @Id
    @GeneratedValue
    @Column(name="id_role")
    private Long id;
    @NotNull
    private int accessLevel;
    @NotNull
    @Column(unique = true)
    private String title;

    public Role() { }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public int getAccessLevel() {
        return accessLevel;
    }

    public void setAccessLevel(int accessLevel) {
        this.accessLevel = accessLevel;
    }

    public String getTitle() { return title; }

    public void setTitle(String title) {
        this.title = title;
    }
}
