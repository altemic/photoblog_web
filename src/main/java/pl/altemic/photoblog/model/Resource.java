package pl.altemic.photoblog.model;

import com.sun.istack.internal.NotNull;

import javax.persistence.*;

/**
 * Created by michal on 16.10.2016.
 */
@Entity(name="resource")
@NamedQueries({
        @NamedQuery(name = "Resource.findByKeyAndDict", query = "SELECT r FROM resource r WHERE r.dict=?1 AND r.key=?2"),
        @NamedQuery(name="Resource.findAllByDict", query = "SELECT r FROM resource r WHERE r.dict=?1"),
        @NamedQuery(name="Resource.findAll", query = "SELECT r FROM resource r"),
        @NamedQuery(name="Resource.findAllDicts", query = "SELECT DISTINCT (d.dict) FROM resource d")
})
public class Resource {

    @Id
    @GeneratedValue
    @Column(name="id_resource")
    private long id;
    @NotNull
    @Column(name = "identifier")
    private String key;
    @NotNull
    private String dict;
    @NotNull
    private String value;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getDict() {
        return dict;
    }

    public void setDict(String dict) {
        this.dict = dict;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
