package pl.altemic.photoblog.model;


import com.sun.istack.internal.NotNull;
import pl.altemic.photoblog.viewmodel.UserViewModel;

import javax.annotation.PostConstruct;
import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by michal on 31.07.2016.
 */
@Entity(name="user")
@NamedQueries(
        {@NamedQuery(name="User.findAll", query = "SELECT u FROM user u"),
        @NamedQuery(name="User.findByUsername", query = "SELECT u FROM user u WHERE u.username=?1")})

public class User {
    @Id
    @GeneratedValue
    @Column(name="id_user")
    private long id;
    private String firstname;
    private String lastname;
    @Column(unique = true)
    @NotNull
    private String username;
    private String email;
    @OneToMany(mappedBy = "user")
    private List<Device> devices;
    @ManyToMany
    @JoinTable(name="user_roles",
        joinColumns = {@JoinColumn(name="user_id", referencedColumnName = "id_user")},
        inverseJoinColumns = {@JoinColumn(name="role_id", referencedColumnName = "id_role")})
    private List<Role> roles;
    @NotNull
    private String password;

    public User(){
        roles = new ArrayList<>();
        devices = new ArrayList<>();
    }

    public String getPassword() { return password; }

    public void setPassword(String password) { this.password = password; }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String nickname) {
        this.username = nickname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public List<Device> getDevices() {
        return devices;
    }

    public void setDevices(List<Device> device) {
        this.devices = device;
    }

    public List<Role> getRoles() { return roles; }

    public void setRoles(List<Role> roles) {
        this.roles = roles;
    }
}
