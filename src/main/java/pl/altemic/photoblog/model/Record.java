package pl.altemic.photoblog.model;

import com.sun.istack.internal.NotNull;
import com.sun.istack.internal.Nullable;
import pl.altemic.photoblog.viewmodel.RecordViewModel;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.List;

/**
 * Created by michal on 31.07.2016.
 */

@Entity(name= "record")
@NamedQueries(
        {@NamedQuery(name="Record.findAll", query = "SELECT r FROM record r ORDER BY r.date DESC"),
        @NamedQuery(name="Record.count", query = "SELECT COUNT (r.id) FROM record r")}
)
public class Record {
    @Id
    @GeneratedValue
    @Column(name = "id_record")
    private long id;
    @OneToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    @JoinColumn(name="picture_id")
    @NotNull
    private Picture picture;
    private String title;
    private String description;
    @ManyToOne
    @JoinColumn(name="user_id")
    @NotNull
    private User user;
    @NotNull
    private Timestamp date;

    public Record() {}

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Picture getPicture() {
        return picture;
    }

    public void setPicture(Picture picture) {
        this.picture = picture;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Timestamp getDate() {
        return date;
    }

    public void setDate(Timestamp timestamp) {
        this.date = timestamp;
    }
}
