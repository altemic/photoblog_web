package pl.altemic.photoblog.model;

import com.sun.org.apache.xerces.internal.impl.dv.util.Base64;
import pl.altemic.photoblog.viewmodel.PictureViewModel;

import javax.persistence.*;

/**
 * Created by michal on 31.07.2016.
 */
@Entity(name = "picture")
public class Picture {
    @Id
    @GeneratedValue
    @Column(name= "id_picture")
    private long id;
    @Column(length = 16777215)
    private byte[] picture;
    private String extension;

    public Picture() {}

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public byte[] getPicture() {
        return picture;
    }

    public void setPicture(byte[] picture) {
        this.picture = picture;
    }

    public String getExtension() { return extension; }

    public void setExtension(String extension) { this.extension = extension; }
}
