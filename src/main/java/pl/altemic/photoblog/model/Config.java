package pl.altemic.photoblog.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.NamedQuery;

/**
 * Created by michal on 28.08.2016.
 */
@Entity(name = "config")
@NamedQuery(name = "Config.findAll", query = "SELECT c FROM config c")
public class Config {

    @Id
    @GeneratedValue
    private long id;
    private String name;
    private String value;

    public String getName() { return name; }

    public void setName(String name) { this.name = name; }

    public String getValue() { return value; }

    public void setValue(String value) { this.value = value; }

    public long getId() { return id; }

    public void setId(long id) { this.id = id; }
}
