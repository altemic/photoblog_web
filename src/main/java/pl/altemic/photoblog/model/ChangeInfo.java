package pl.altemic.photoblog.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.lang.reflect.Field;
import java.sql.Timestamp;
import java.util.Calendar;

/**
 * Created by michal on 18.08.2016.
 */
@Entity(name = "change_info")
public class ChangeInfo {
    @Id
    @GeneratedValue
    @Column(name="id_change_info")
    private long id;
    private String description;
    private String tableName;
    private Timestamp date;
    private long source_id;

    public ChangeInfo() { }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getTableName() {
        return tableName;
    }

    public void setTableName(String table) {
        this.tableName = table;
    }

    public Timestamp getDate() {
        return date;
    }

    public void setDate(Timestamp date) { this.date = date; }

    public long getSource_id() { return source_id; }

    public void setSource_id(long source_id) { this.source_id = source_id; }
}
