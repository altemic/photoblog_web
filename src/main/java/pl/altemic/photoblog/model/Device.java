package pl.altemic.photoblog.model;

import pl.altemic.photoblog.viewmodel.DeviceViewModel;

import javax.persistence.*;

/**
 * Created by michal on 31.07.2016.
 */
@Entity(name = "device")
public class Device {
    @Id
    @GeneratedValue
    @Column(name="id_device")
    private long id;
    private String name;
    @ManyToOne
    @JoinColumn(name = "user_id")
    private User user;

    public Device() {}

    public long getId() { return id; }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
