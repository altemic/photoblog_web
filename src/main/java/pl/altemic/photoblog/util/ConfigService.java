package pl.altemic.photoblog.util;

import javax.ejb.Local;

/**
 * Created by michal on 16.09.2016.
 */

@Local
public interface ConfigService {
    ConfigScheme load();
    void save(ConfigScheme cfg);
}
