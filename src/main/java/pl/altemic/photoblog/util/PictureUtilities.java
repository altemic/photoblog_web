package pl.altemic.photoblog.util;

import org.imgscalr.Scalr;

import javax.ejb.Stateless;
import javax.imageio.ImageIO;
import java.awt.geom.AffineTransform;
import java.awt.image.AffineTransformOp;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;

/**
 * Created by michal on 08.09.2016.
 */

@Stateless
public class PictureUtilities {
    public byte[] rotate(byte[] inputArr, int degrees, String extension) throws IOException {
        ByteArrayInputStream inputStream = new ByteArrayInputStream(inputArr);
        BufferedImage bufferedImage = ImageIO.read(inputStream);
        BufferedImage newImage;

        switch (degrees % 360){
            case 0:
                newImage = bufferedImage;
                break;
            case 90:
                newImage = Scalr.rotate(bufferedImage, Scalr.Rotation.CW_90);
                break;
            case 180:
                newImage = Scalr.rotate(bufferedImage, Scalr.Rotation.CW_180);
                break;
            case 270:
                newImage = Scalr.rotate(bufferedImage, Scalr.Rotation.CW_270);
                break;
            default:
                newImage = affineRotate(bufferedImage, degrees);
                break;
        }

        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        ImageIO.write(newImage, extension, outputStream);
        return outputStream.toByteArray();
    }

    public BufferedImage affineRotate(BufferedImage initial, double degrees){
        AffineTransform tx = new AffineTransform();
        tx.translate(initial.getWidth() / 2, initial.getHeight() / 2);
        tx.rotate(Math.toRadians(degrees));
        tx.translate(-initial.getHeight() / 2,-initial.getWidth() / 2);
        AffineTransformOp op = new AffineTransformOp(tx, AffineTransformOp.TYPE_NEAREST_NEIGHBOR);
        BufferedImage newImage = new BufferedImage(initial.getHeight(), initial.getWidth(), initial.getType());
        op.filter(initial, newImage);
        return newImage;
    }
}
