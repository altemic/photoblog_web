package pl.altemic.photoblog.util;

import pl.altemic.photoblog.dao.ChangeInfoDao;
import pl.altemic.photoblog.model.ChangeInfo;

import javax.inject.Inject;
import javax.interceptor.AroundInvoke;
import javax.interceptor.Interceptor;
import javax.interceptor.InvocationContext;
import javax.persistence.Entity;
import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.sql.Timestamp;
import java.util.Calendar;

/**
 * Created by michal on 01.08.2016.
 */


//Use Transactional from java ee 7
@RegisterChangeInfo
@Interceptor
public class ChangeInfoRegisterer {

    private ChangeInfoDao changeInfoDao;

    @Inject
    public ChangeInfoRegisterer(ChangeInfoDao _changeInfoDao){
        this.changeInfoDao = _changeInfoDao;
    }

    @AroundInvoke
    public Object aroundInvoke(InvocationContext ictx) throws Exception {
        try {
            return ictx.proceed();
        } finally {
            Calendar cal = Calendar.getInstance();
            long timeMillis = cal.getTimeInMillis();
            Timestamp date = new Timestamp(timeMillis);

            Object[] parameters = ictx.getParameters();
            Object o = parameters[0];

            Type genericSuperClass = ictx.getMethod().getDeclaringClass().getGenericSuperclass();
            Annotation annotation;
            if(!genericSuperClass.equals(Object.class)) {
                ParameterizedType parametrizedType = null;
                while (parametrizedType == null) {
                    if ((genericSuperClass instanceof ParameterizedType)) {
                        parametrizedType = (ParameterizedType) genericSuperClass;
                    } else {
                        genericSuperClass = ((Class<?>) genericSuperClass).getGenericSuperclass();
                    }
                }
                Type t = parametrizedType.getActualTypeArguments()[0];
                annotation = ((Class) t).getAnnotation(Entity.class);
            } else {
                annotation = o.getClass().getAnnotation(Entity.class);
            }
            Entity entity = (Entity) annotation;
            String entityName = entity.name();
            String methodName = ictx.getMethod().getName();

            long id = -1;
            if (methodName.equals("remove")) {
                id = (long) o;
            } else {
                Field field = o.getClass().getDeclaredField("id");
                field.setAccessible(true);
                id = (long) field.get(o);
            }

            ChangeInfo changeInfo = new ChangeInfo();
            changeInfo.setDate(date);
            changeInfo.setDescription(methodName);
            changeInfo.setSource_id(id);
            changeInfo.setTableName(entityName);
            changeInfoDao.save(changeInfo);
        }
    }
}
