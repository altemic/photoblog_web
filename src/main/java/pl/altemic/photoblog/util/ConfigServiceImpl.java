package pl.altemic.photoblog.util;

import pl.altemic.photoblog.dao.ConfigDao;
import pl.altemic.photoblog.model.Config;

import javax.ejb.Stateless;
import javax.inject.Inject;
import java.io.Serializable;
import java.lang.reflect.Field;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by michal on 28.08.2016.
 */

@Stateless
public class ConfigServiceImpl implements ConfigService, Serializable{

    private ConfigDao configDao;
    private ConfigScheme cfgScheme;

    @Inject
    public ConfigServiceImpl(ConfigDao _configDao){
        this.configDao = _configDao;
    }

    public ConfigScheme load() {
        List<Config> cfgList = configDao.findAll();
        cfgScheme = new ConfigScheme();

        Field[] cfgFields = cfgScheme.getClass().getDeclaredFields();

        for (Field cfgField : cfgFields) {
            cfgField.setAccessible(true);
            String name = cfgField.getName();
            List<Config> matchingEntries = cfgList.stream().filter(c -> c.getName().equals(name)).collect(Collectors.toList());
            if (matchingEntries.size() == 1) {
                try {
                    String value = matchingEntries.get(0).getValue();
                    cfgField.set(cfgScheme, value);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }

        if(cfgScheme.getTimesRun().equals(1)) {
            save(cfgScheme);
        }

        return cfgScheme;
    }

    public void save(ConfigScheme cfgScheme) {
        try {
            List<Config> cfgStoredInDb = configDao.findAll();
            Field[] cfgFields = cfgScheme.getClass().getDeclaredFields();
            for (Field cfgField : cfgFields) {
                cfgField.setAccessible(true);
                String name = cfgField.getName();
                List<Config> matchingEntries = cfgStoredInDb.stream().filter(c -> c.getName().equals(name)).collect(Collectors.toList());
                try {
                    String value = (String) cfgField.get(cfgScheme);
                    if (matchingEntries.size() == 1) {
                        Config cfgEntry = matchingEntries.get(0);
                        cfgEntry.setValue(value);
                        configDao.update(cfgEntry);

                    } else if (matchingEntries.size() == 0) {
                        Config newCfgEntry = new Config();
                        newCfgEntry.setName(name);
                        newCfgEntry.setValue(value);
                        configDao.save(newCfgEntry);
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        } catch (Exception e){
            e.printStackTrace();
        }
    }
}
