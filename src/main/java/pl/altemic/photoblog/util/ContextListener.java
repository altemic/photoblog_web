package pl.altemic.photoblog.util;

import pl.altemic.photoblog.dao.ConfigDao;
import pl.altemic.photoblog.dao.ResourceDao;
import pl.altemic.photoblog.dao.RoleDao;
import pl.altemic.photoblog.dao.UserDao;
import pl.altemic.photoblog.model.Resource;
import pl.altemic.photoblog.model.Role;
import pl.altemic.photoblog.model.User;

import javax.inject.Inject;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Created by michal on 28.08.2016.
 */
@WebListener
public class ContextListener implements ServletContextListener{

    @Inject
    private ConfigDao configDao;

    @Inject
    private RoleDao roleDao;

    @Inject
    private UserDao userDao;

    @Inject
    private ResourceDao resourceDao;

    @Inject
    private ConfigService configService;

    @Override
    public void contextInitialized(ServletContextEvent servletContextEvent) {

       ConfigScheme settings = configService.load();
        if(Integer.valueOf(settings.getTimesRun()) >= 1) return;

        Map<Integer, Init> initParts = new LinkedHashMap();
        initParts.put(1, () -> addRoles());
        initParts.put(2, () -> addUsers());
        initParts.put(3, () -> addResources());

        Initialise(initParts);

        Integer timesRun = Integer.valueOf(settings.getTimesRun());
        timesRun++;
        settings.setTimesRun(String.valueOf(timesRun));

        configService.save(settings);
    }

    @Override
    public void contextDestroyed(ServletContextEvent servletContextEvent) {

    }

    private void Initialise(Map<Integer, Init> steps){
        Map<Integer, Init> orderedMap =
                steps.entrySet().stream().sorted(Map.Entry.comparingByKey(
                        (o1, o2) -> o1 > o2 ? 1 : -1
                        )).collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));

        for (Map.Entry step : orderedMap.entrySet()){
            Init obj = (Init)step.getValue();
            boolean success = obj.Execute();
            if(!success) return;
        }
    }

    private interface Init{
        boolean Execute();
    }

    private boolean addRoles() {
        try {
            long adminRoles = roleDao.findAll().stream().filter(r -> r.getTitle().equals("admin")).count();

            if (adminRoles == 0) {
                Role adminRole = new Role();
                adminRole.setTitle("admin");
                adminRole.setAccessLevel(1);
                try {
                    roleDao.save(adminRole);
                } catch (Exception e){
                    e.printStackTrace();
                }
            }

            long userRoles = roleDao.findAll().stream().filter(r -> r.getTitle().equals("user")).count();

            if (userRoles == 0) {
                Role userRole = new Role();
                userRole.setTitle("user");
                userRole.setAccessLevel(2);
                roleDao.save(userRole);
            }

            return true;
        } catch (Exception e){
            e.printStackTrace();
            return false;
        }
    }

    private boolean addUsers(){
        try {
            long adminUsers = userDao.findAll().stream().filter(u -> u.getUsername().equals("admin")).count();

            if (adminUsers == 0) {
                Role adminRole = roleDao.findAll().stream().filter(r -> r.getTitle().equals("admin")).collect(Collectors.toList()).get(0);
                User adminUser = new User();
                adminUser.setUsername("admin");
                adminUser.setPassword("P@ssw0rd");
                List<Role> roleList = new ArrayList<>();
                roleList.add(adminRole);
                adminUser.setRoles(roleList);
                userDao.save(adminUser);
            }

            return true;
        } catch (Exception e){
            e.printStackTrace();
            return false;
        }
    }

    private boolean addResources(){
        try{
            //Create only english dict - if more needed maybe file import would be better

            String dictTAG = "ENG";
            HashSet<String> set = new HashSet<>();
            set.add("Login");
            set.add("Sign in");
            set.add("Logout");
            set.add("Main page");
            set.add("Edit");
            set.add("Delete");
            set.add("Posted by");
            set.add("New");
            set.add("Control panel");
            set.add("Add record");
            set.add("Edit record");
            set.add("Title");
            set.add("Image");
            set.add("Description");
            set.add("Update");
            set.add("Add");
            set.add("Rotate");
            set.add("Please sign in");
            set.add("Username");
            set.add("First name");
            set.add("Last name");
            set.add("Password");
            set.add("Next");
            set.add("Previous");
            set.add("Modify user");
            set.add("Add user");
            set.add("Roles");
            set.add("Action");
            set.add("User list");
            set.add("Field");
            set.add("Dict");
            set.add("Key");
            set.add("Value");
            set.add("Resource management");
            set.add("Resources");
            set.add("Users");
            set.add("Add language support");

            for(String entry : set){
                Resource res = new Resource();
                res.setDict(dictTAG);
                res.setValue(entry);
                res.setKey(entry);
                resourceDao.save(res);
            }

            return true;
        } catch (Exception e){
            e.printStackTrace();
            return false;
        }
    }
}
