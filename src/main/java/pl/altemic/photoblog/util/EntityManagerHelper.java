package pl.altemic.photoblog.util;

import javax.enterprise.inject.Produces;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.TransactionScoped;

/**
 * Created by michal on 01.08.2016.
 */
public class EntityManagerHelper {

    @PersistenceContext(unitName = "piesekfiona_persistenceunit")
    private EntityManager em;

    @Produces
    @TransactionScoped
    public EntityManager createEntityManager() {
        return em;
    }
}
