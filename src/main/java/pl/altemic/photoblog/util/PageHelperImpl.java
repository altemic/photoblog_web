package pl.altemic.photoblog.util;

import javax.annotation.PostConstruct;
import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;
import java.io.Serializable;

/**
 * Created by michal on 11.09.2016.
 */
@SessionScoped
public class PageHelperImpl implements PageHelper, Serializable {

    private ConfigService configService;
    private int recordsPerPage;
    private int pageNo;

    @Inject
    public PageHelperImpl(ConfigService _configService){
        this.configService = _configService;
    }

    @PostConstruct
    public void init(){
        ConfigScheme cfg = configService.load();
        setRecordsPerPage(Integer.valueOf(cfg.getRecordsPerPage()));
        this.pageNo = 1;
    }

    public int getRecordsPerPage() {
        return recordsPerPage;
    }

    public void setRecordsPerPage(int recordsPerPage) {
        this.recordsPerPage = recordsPerPage;
    }

    @Override
    public int getPage() {
        return this.pageNo;
    }

    @Override
    public void setPage(int page) {
        this.pageNo = page;
    }
}
