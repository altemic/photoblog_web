package pl.altemic.photoblog.util;

/**
 * Created by michal on 17.09.2016.
 */
public interface PageHelper {
    int getRecordsPerPage();
    int getPage();
    void setPage(int page);
    void setRecordsPerPage(int recordsPerPage);
}
