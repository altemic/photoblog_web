package pl.altemic.photoblog.util;

import java.io.Serializable;
import java.lang.reflect.Field;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by michal on 28.08.2016.
 */

public class ConfigScheme implements Serializable{
    //Only string fields are supported
    private String launchDate;
    private String timesRun;
    private String recordsPerPage;
    private String defLanguage;
    private String language;
    private String pageName;

    public ConfigScheme() {
        timesRun = String.valueOf(0);
        launchDate = new Timestamp(Calendar.getInstance().getTimeInMillis()).toString();
        recordsPerPage = "15";
        defLanguage = "ENG";
        language = "ENG";
        pageName = "piesekfiona.pl";
    }

    public void SetField(String fieldName, String newValue) throws NoSuchFieldException {
        Field f = this.getClass().getDeclaredField(fieldName);
        if(f != null) {
            try {
                f.setAccessible(true);
                f.set(this, newValue);
            } catch (IllegalAccessException ex){
                ex.printStackTrace();
            }
        }
    }

    public Map<String, String> getAll() throws IllegalAccessException {
        Map<String, String> fieldMap = new HashMap<>();
        for(Field f : this.getClass().getDeclaredFields()){
            f.setAccessible(true);
            String fieldName = f.getName();
            String fieldValue = (String) f.get(this);
            fieldMap.put(fieldName, fieldValue);
        }

        return fieldMap;
    }

    public String getLaunchDate() {
        return launchDate;
    }

    public void setLaunchDate(String launchDate) {
        this.launchDate = launchDate;
    }

    public String getTimesRun() {
        return timesRun;
    }

    public void setTimesRun(String timesRun) {
        this.timesRun = timesRun;
    }

    public String getRecordsPerPage() { return recordsPerPage;  }

    public void setRecordsPerPage(String recordsPerPage) { this.recordsPerPage = recordsPerPage; }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getPageName() {
        return pageName;
    }

    public void setPageName(String pageName) {
        this.pageName = pageName;
    }

    public String getDefLanguage() { return defLanguage; }

    public void setDefLanguage(String defLanguage) {
        this.defLanguage = defLanguage;
    }
}
