package pl.altemic.photoblog.dao;

import java.util.List;
import java.util.Map;

/**
 * Created by michal on 02.08.2016.
 */
public interface BaseCRUD<T> {
    void save(T obj);
    void remove(Long id);
    T update(T obj);
    T get(Long id);
    List<T> customQuery(String customQuery, Map<String, Object> params);
    List<T> namedQuery(String namedQuery, Map<String, Object> params);
}
