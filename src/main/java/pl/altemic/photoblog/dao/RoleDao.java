package pl.altemic.photoblog.dao;

import pl.altemic.photoblog.model.Role;

import java.util.List;
import java.util.Map;

/**
 * Created by michal on 18.08.2016.
 */
public interface RoleDao extends BaseCRUD<Role> {
    public List<Role> findAll();
    public Role findRoleByTitle(String name);
    public List<Role> customQuery(String customQuery, Map<String, Object> params);
    public List<Role> namedQuery(String namedQuery, Map<String, Object> params);
}
