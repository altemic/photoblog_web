package pl.altemic.photoblog.dao;

import pl.altemic.photoblog.model.Resource;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by michal on 16.10.2016.
 */
@Stateless
public class ResourceDaoImpl extends BaseCRUDImpl<Resource> implements ResourceDao {
    @Inject
    public ResourceDaoImpl(EntityManager em){ super(em); }

    @Override
    public Resource findByKeyAndDict(String dict, String key) {
        TypedQuery<Resource> query = em.createNamedQuery("Resource.findByKeyAndDict", Resource.class);
        Map<String, Object> params = new HashMap();
        params.put("1", dict);
        params.put("2", key);
        insertParameters(query, params);
        List<Resource> resourceList = query.getResultList();
        if(resourceList.isEmpty()) return null;
        return resourceList.get(0);
    }

    @Override
    public List<Resource> findAll() {
        TypedQuery<Resource> query = em.createNamedQuery("Resource.findAll", Resource.class);
        List<Resource> result = query.getResultList();
        return result;
    }

    @Override
    public List<Resource> findAllByDict(String dict) {
        TypedQuery<Resource> query = em.createNamedQuery("Resource.findAllByDict", Resource.class);
        Map<String, Object> params = new HashMap();
        params.put("1", dict);
        insertParameters(query, params);
        List<Resource> resource = query.getResultList();
        return resource;
    }

    @Override
    public List<String> findAllDicts() {
        Query query = em.createNamedQuery("Resource.findAllDicts");
        List<String> dictionaries = query.getResultList();
        return dictionaries;
    }
}
