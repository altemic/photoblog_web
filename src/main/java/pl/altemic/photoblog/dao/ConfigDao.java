package pl.altemic.photoblog.dao;

import pl.altemic.photoblog.model.Config;

import java.util.List;

/**
 * Created by michal on 28.08.2016.
 */
public interface ConfigDao extends BaseCRUD<Config> {
    List<Config> findAll();
}
