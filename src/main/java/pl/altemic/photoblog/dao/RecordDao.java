package pl.altemic.photoblog.dao;

import pl.altemic.photoblog.model.Record;

import java.util.List;
import java.util.Map;

/**
 * Created by michal on 01.08.2016.
 */
public interface RecordDao extends BaseCRUD<Record> {
    List<Record> findAll();
    List<Record> getPagedRrecords(int page, int recordNO);
    int countRecords();
    List<Record> customQuery(String customQuery, Map<String, Object> params);
    List<Record> namedQuery(String customQuery, Map<String, Object> params);
}
