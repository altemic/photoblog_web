package pl.altemic.photoblog.dao;

import pl.altemic.photoblog.model.Device;
import pl.altemic.photoblog.model.User;
import pl.altemic.photoblog.util.RegisterChangeInfo;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;

/**
 * Created by michal on 01.08.2016.
 */
@Stateless
public class DeviceDaoImpl extends BaseCRUDImpl<Device> implements DeviceDao {

    private UserDao userDao;

    @Inject
    public DeviceDaoImpl(EntityManager em){
        super(em);
        userDao = new UserDaoImpl(em);
    }

    @Override
    @RegisterChangeInfo
    public void save(Device device) {
        User userFromDb = userDao.get(device.getUser().getId());
        device.setUser(userFromDb);
        em.persist(device);
    }

    @Override
    @RegisterChangeInfo
    public Device update(Device device) {
        User userFromDb = userDao.get(device.getUser().getId());
        device.setUser(userFromDb);
        return em.merge(device);
    }
}
