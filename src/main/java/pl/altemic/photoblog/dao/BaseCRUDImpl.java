package pl.altemic.photoblog.dao;

import pl.altemic.photoblog.util.RegisterChangeInfo;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import java.io.Serializable;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.List;
import java.util.Map;

/**
 * Created by michal on 18.08.2016.
 */
@Stateless
public abstract class BaseCRUDImpl<T> implements BaseCRUD<T>, Serializable{

    protected EntityManager em;

    private Class<T> entityClass;

    @Inject
    public BaseCRUDImpl(EntityManager em) {
        this.em = em;
        Type genericSuperClass = getClass().getGenericSuperclass();
        ParameterizedType parametrizedType = null;
        while (parametrizedType == null) {
            if ((genericSuperClass instanceof ParameterizedType)) {
                parametrizedType = (ParameterizedType) genericSuperClass;
            } else {
                genericSuperClass = ((Class<?>) genericSuperClass).getGenericSuperclass();
            }
        }

        entityClass = (Class<T>) parametrizedType.getActualTypeArguments()[0];
    }

    public void setEntityManager(EntityManager emX){
        em = emX;
    }

    @Override
    @RegisterChangeInfo
    public void save(T obj) {
        em.persist(obj);
    }

    @Override
    @RegisterChangeInfo
    public void remove(Long id) {
        T toDelete = em.find(entityClass, id);
        if(toDelete != null) em.remove(toDelete);
    }

    @Override
    @RegisterChangeInfo
    public T update(T device) {
        return em.merge(device);
    }

    @Override
    public T get(Long id) {
        return em.find(entityClass, id);
    }

    @Override
    public List<T> customQuery(String customQuery, Map<String, Object> params) {
        TypedQuery<T> query = em.createQuery(customQuery, entityClass);
        insertParameters(query, params);
        List<T> result = query.getResultList();
        return result;
    }

    @Override
    public List<T> namedQuery(String namedQuery, Map<String, Object> params) {
        TypedQuery<T> query = em.createNamedQuery(namedQuery, entityClass);
        insertParameters(query, params);
        List<T> result = query.getResultList();
        return result;
    }

    public void insertParameters(Query query, Map<String, Object> params){
        if(params!=null){
            for(String paramName : params.keySet()){
                Object param = params.get(paramName);
                query.setParameter(paramName, param);
            }
        }
    }
}
