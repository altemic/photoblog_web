package pl.altemic.photoblog.dao;

import pl.altemic.photoblog.model.Record;
import pl.altemic.photoblog.model.User;
import pl.altemic.photoblog.util.RegisterChangeInfo;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import java.util.List;

/**
 * Created by michal on 01.08.2016.
 */
@Stateless
public class RecordDaoImpl extends BaseCRUDImpl<Record> implements RecordDao {

    private PictureDao pictureDao;
    private UserDao userDao;

    @Inject
    public RecordDaoImpl(EntityManager em){
        super(em);
        this.pictureDao = new PictureDaoImpl(em);
        this.userDao = new UserDaoImpl(em);
    }

    @Override
    @RegisterChangeInfo
    public void save(Record record) {
        User user = userDao.get(record.getUser().getId());
        pictureDao.save(record.getPicture());
        record.setUser(user);
        em.persist(record);
    }

    @Override
    @RegisterChangeInfo
    public Record update(Record record) {
        return em.merge(record);
    }

    @Override
    public void remove(Long id){
        Record record = em.find(Record.class, id);
        if(record!=null){
            em.remove(record);
        }
    }

    public List<Record> getPagedRrecords(int page, int recordNO){
        TypedQuery<Record> query = em.createNamedQuery("Record.findAll", Record.class);
        query.setFirstResult((page-1) * recordNO);
        query.setMaxResults(recordNO);
        List<Record> result = query.getResultList();
        return result;
    }

    @Override
    public int countRecords(){
        int count = ((Number)em.createNamedQuery("Record.count").getSingleResult()).intValue();
        return count;
    }

    @Override
    public List<Record> findAll() {
        TypedQuery<Record> query = em.createNamedQuery("Record.findAll", Record.class);
        List<Record> result = query.getResultList();
        return result;
    }
}
