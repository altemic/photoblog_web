package pl.altemic.photoblog.dao;

import pl.altemic.photoblog.model.User;

import java.util.List;

/**
 * Created by michal on 01.08.2016.
 */
public interface UserDao extends BaseCRUD<User>{
    List<User> findAll();
    User findByUsername(String name);
}
