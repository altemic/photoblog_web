package pl.altemic.photoblog.dao;

import pl.altemic.photoblog.model.Resource;

import java.util.List;

/**
 * Created by michal on 16.10.2016.
 */
public interface ResourceDao extends BaseCRUD<Resource> {
    Resource findByKeyAndDict(String dict, String key);
    List<Resource> findAll();
    List<Resource> findAllByDict(String dict);
    List<String> findAllDicts();
}
