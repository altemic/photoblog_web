package pl.altemic.photoblog.dao;

import pl.altemic.photoblog.model.ChangeInfo;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import java.util.List;
import java.util.Map;

/**
 * Created by michal on 21.08.2016.
 * Class does not extend BaseCRUDImpl, becouse it is not wanted to mark methods with RegisterChangeInfo interceptor
 */
@Stateless
public class ChangeInfoDaoImpl implements ChangeInfoDao {

    private EntityManager em;

    @Inject
    public ChangeInfoDaoImpl(EntityManager em){
        this.em = em;
    }

    @Override
    public void save(ChangeInfo obj) {
        em.persist(obj);
    }

    @Override
    public void remove(Long id) {
        ChangeInfo changeInfo = em.find(ChangeInfo.class, id);
        em.remove(changeInfo);
    }

    @Override
    public ChangeInfo update(ChangeInfo obj) { return em.merge(obj); }

    @Override
    public ChangeInfo get(Long id) {
        return em.find(ChangeInfo.class, id);
    }

    @Override
    public List<ChangeInfo> customQuery(String customQuery, Map<String, Object> params) {
        throw new NotImplementedException();
    }

    @Override
    public List<ChangeInfo> namedQuery(String namedQuery, Map<String, Object> params) {
        throw new NotImplementedException();
    }
}
