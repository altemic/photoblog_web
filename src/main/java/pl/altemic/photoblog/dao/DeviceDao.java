package pl.altemic.photoblog.dao;

import pl.altemic.photoblog.model.Device;

/**
 * Created by michal on 01.08.2016.
 */
public interface DeviceDao extends BaseCRUD<Device> {
}
