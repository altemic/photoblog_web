package pl.altemic.photoblog.dao;

import pl.altemic.photoblog.model.Picture;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import java.io.Serializable;

/**
 * Created by michal on 01.08.2016.
 */
@Stateless
public class PictureDaoImpl extends BaseCRUDImpl<Picture> implements PictureDao, Serializable{

    @Inject
    public PictureDaoImpl(EntityManager em){
        super(em);
    }
}
