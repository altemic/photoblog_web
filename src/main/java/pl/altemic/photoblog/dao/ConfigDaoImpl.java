package pl.altemic.photoblog.dao;

import pl.altemic.photoblog.model.Config;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import java.util.List;

/**
 * Created by michal on 28.08.2016.
 */
@Stateless
public class ConfigDaoImpl extends BaseCRUDImpl<Config> implements ConfigDao {

    @Inject
    public ConfigDaoImpl(EntityManager em){
        super(em);
    }

    @Override
    public List<Config> findAll() {
        TypedQuery<Config> query = em.createNamedQuery("Config.findAll", Config.class);
        List<Config> result = query.getResultList();
        return result;
    }
}
