package pl.altemic.photoblog.dao;

import pl.altemic.photoblog.model.Role;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by michal on 18.08.2016.
 */
@Stateless
public class RoleDaoImpl extends BaseCRUDImpl<Role> implements RoleDao {

    @Inject
    public RoleDaoImpl(EntityManager em){
        super(em);
    }

    public List<Role> findAll(){
        List<Role> roleList = null;
        TypedQuery<Role> query = em.createNamedQuery("Role.findAll", Role.class);
        roleList = query.getResultList();
        return roleList;
    }

    public Role findRoleByTitle(String name) {
        TypedQuery<Role> query = em.createNamedQuery("Role.findByName", Role.class);
        Map<String, Object> params = new HashMap();
        params.put("1", name);
        insertParameters(query, params);
        Role role = query.getSingleResult();
        return role;
    }
}
