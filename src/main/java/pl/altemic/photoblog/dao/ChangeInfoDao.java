package pl.altemic.photoblog.dao;

import pl.altemic.photoblog.model.ChangeInfo;

/**
 * Created by michal on 21.08.2016.
 */
public interface ChangeInfoDao extends BaseCRUD<ChangeInfo> {
}
