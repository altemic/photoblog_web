package pl.altemic.photoblog.dao;

import pl.altemic.photoblog.model.Picture;

/**
 * Created by michal on 01.08.2016.
 */
public interface PictureDao extends BaseCRUD<Picture> {
}
