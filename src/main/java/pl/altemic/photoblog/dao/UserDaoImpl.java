package pl.altemic.photoblog.dao;

import pl.altemic.photoblog.model.Role;
import pl.altemic.photoblog.model.User;
import pl.altemic.photoblog.util.RegisterChangeInfo;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by michal on 01.08.2016.
 */
@Stateless
public class UserDaoImpl extends BaseCRUDImpl<User> implements UserDao {

    private RoleDao roleDao;

    @Inject
    public UserDaoImpl(EntityManager em){
        super(em);
        roleDao = new RoleDaoImpl(em);
    }

    @Override
    @RegisterChangeInfo
    public void save(User user) {
        user.setRoles(getRolesForUser(user));
        em.persist(user);
    }

    @Override
    @RegisterChangeInfo
    public User update(User user) {
        user.setRoles(getRolesForUser(user));
        return em.merge(user);
    }

    @Override
    @RegisterChangeInfo
    public void remove(Long id) {
        User user = em.find(User.class, id);
        if(user != null){
            em.remove(user);
        }
    }

    @Override
    public List<User> findAll() {
        TypedQuery<User> query = em.createNamedQuery("User.findAll", User.class);
        List<User> userList = query.getResultList();
        return userList;
    }

    @Override
    public User findByUsername(String name) {
        TypedQuery<User> query = em.createNamedQuery("User.findByUsername", User.class);
        Map<String, Object> params = new HashMap();
        params.put("1", name);
        insertParameters(query, params);
        List<User> userList = query.getResultList();
        return userList.get(0);
    }

    private List<Role> getRolesForUser(User user){
        List<Role> roleList = new ArrayList<>();
        for(Role r : user.getRoles()){
            Role roleFromDb = roleDao.get(r.getId());
            if(roleFromDb != null) roleList.add(roleFromDb);
        }
        return roleList;
    }
}
