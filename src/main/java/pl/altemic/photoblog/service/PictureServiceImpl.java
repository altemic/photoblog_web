package pl.altemic.photoblog.service;

import pl.altemic.photoblog.dao.PictureDao;
import pl.altemic.photoblog.dto.PictureDto;
import pl.altemic.photoblog.model.Picture;

import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;

/**
 * Created by michal on 29.08.2016.
 */
@SessionScoped
public class PictureServiceImpl extends BaseCRUDServiceImpl<PictureDto, Picture> implements PictureService {

    private PictureDao pictureDao;

    @Inject
    public PictureServiceImpl(PictureDao _pictureDao){
        this.pictureDao = _pictureDao;
    }

    @Override
    public void save(PictureDto obj) {
        Picture picture = FromDtoToModel(obj);
        pictureDao.save(picture);
    }

    @Override
    public void remove(Long id) {
        pictureDao.remove(id);
    }

    @Override
    public PictureDto update(PictureDto obj) {
        Picture picture = FromDtoToModel(obj);
        return  FromModelToDto(pictureDao.update(picture));
    }

    @Override
    public PictureDto get(Long id) {
        return FromModelToDto(pictureDao.get(id));
    }

    public PictureDto FromModelToDto(Picture picture){
        PictureDto pictureDto = new PictureDto();
        pictureDto.setId(picture.getId());
        pictureDto.setPicture(picture.getPicture());
        pictureDto.setExtension(picture.getExtension());
        return pictureDto;
    }

    public Picture FromDtoToModel(PictureDto pictureDto){
        Picture picture = new Picture();
        picture.setPicture(pictureDto.getPicture());
        picture.setId(pictureDto.getId());
        picture.setExtension(pictureDto.getExtension());
        return picture;
    }
}
