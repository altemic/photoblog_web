package pl.altemic.photoblog.service;

import pl.altemic.photoblog.dao.UserDao;
import pl.altemic.photoblog.dto.RoleDto;
import pl.altemic.photoblog.dto.UserDto;
import pl.altemic.photoblog.model.Role;
import pl.altemic.photoblog.model.User;

import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by michal on 28.08.2016.
 */
@SessionScoped
public class UserServiceImpl extends BaseCRUDServiceImpl<UserDto, User> implements UserService {

    private UserDao userDao;
    private BaseCRUDServiceImpl<RoleDto, Role> roleService;

    @Inject
    public UserServiceImpl(UserDao _userDao, BaseCRUDServiceImpl<RoleDto, Role> _roleService) {
        this.userDao = _userDao;
        this.roleService = _roleService;
    }

    @Override
    public void save(UserDto userViewModel) {
        User user = FromDtoToModel(userViewModel);
        userDao.save(user);
    }

    @Override
    public void remove(Long id) {
        userDao.remove(id);
    }

    @Override
    public UserDto update(UserDto userViewModel) {
        User user = FromDtoToModel(userViewModel);
        return FromModelToDto(userDao.update(user));
    }

    @Override
    public UserDto get(Long id) {
        return FromModelToDto(userDao.get(id));
    }

    @Override
    public List<UserDto> findAll() {
        return userDao.findAll().stream().map(u -> FromModelToDto(u)).collect(Collectors.toList());
    }

    @Override
    public UserDto findByName(String username) {
        return FromModelToDto(userDao.findByUsername(username));
    }

    @Override
    public User FromDtoToModel(UserDto userDto) {
        User user = new User();
        user.setId(userDto.getId());
        user.setPassword(userDto.getPassword());
        user.setUsername(userDto.getUsername());
        user.setFirstname(userDto.getFirstname());
        user.setLastname(userDto.getLastname());
        user.setEmail(userDto.getEmail());
        user.setRoles(userDto.getRoles().stream().map(r -> roleService.FromDtoToModel(r)).collect(Collectors.toList()));
        return user;
    }

    @Override
    public UserDto FromModelToDto(User user) {
        UserDto userViewModel = new UserDto();
        userViewModel.setId(user.getId());
        userViewModel.setUsername(user.getUsername());
        userViewModel.setEmail(user.getEmail());
        userViewModel.setFirstname(user.getFirstname());
        userViewModel.setLastname(user.getLastname());
        userViewModel.setPassword(user.getPassword());
        userViewModel.setRoles(user.getRoles().stream().map(r -> roleService.FromModelToDto(r)).collect(Collectors.toList()));
        return userViewModel;
    }
}
