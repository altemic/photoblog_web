package pl.altemic.photoblog.service;

/**
 * Created by michal on 28.08.2016.
 */
public interface BaseCRUDService<T> {
    void save(T obj);
    void remove(Long id);
    T update(T obj);
    T get(Long id);
}
