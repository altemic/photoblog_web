package pl.altemic.photoblog.service;

import pl.altemic.photoblog.dao.ResourceDao;
import pl.altemic.photoblog.dto.Mapper;
import pl.altemic.photoblog.dto.ResourceDto;
import pl.altemic.photoblog.model.Resource;
import pl.altemic.photoblog.util.ConfigScheme;
import pl.altemic.photoblog.util.ConfigService;

import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by michal on 16.10.2016.
 */
@RequestScoped
public class ResourceServiceImpl extends BaseCRUDServiceImpl<ResourceDto, Resource> implements ResourceService {

    private ResourceDao resourceDao;
    private ConfigService configService;
    private String LANG;
    private String LANG2;
    private final String LanguageNotSetException = "Language was not set before calling method";

    @Inject
    public ResourceServiceImpl(ResourceDao _resourceDao, ConfigService _configService){
        this.resourceDao = _resourceDao;
        this.configService = _configService;
    }

    public void setLanguage(String language){
        LANG = language;
    }

    @Override
    public List<String> findAllDicts() {
        return resourceDao.findAllDicts();
    }

    @PostConstruct
    public void Initialize(){
        ConfigScheme cfgScheme = configService.load();
        LANG = cfgScheme.getLanguage();
        LANG2 = cfgScheme.getDefLanguage();
    }

    @Override
    public ResourceDto findByKeyAndDict(String dict, String key) {
        Resource res = resourceDao.findByKeyAndDict(dict, key);
        return FromModelToDto(resourceDao.findByKeyAndDict(dict, key));
    }

    @Override
    public List<ResourceDto> findAll() {
        return resourceDao.findAll().stream().map(r -> FromModelToDto(r)).collect(Collectors.toList());
    }

    @Override
    public List<ResourceDto> findAllByDict(String dict) {
        return resourceDao.findAllByDict(dict).stream().map(r -> FromModelToDto(r)).collect(Collectors.toList());
    }

    @Override
    public String findValueByKeyAndDict(String dict, String key){
        return findByKeyAndDict(dict, key).getValue();
    }

    @Override
    public ResourceDto findByKey(String key) throws ResourceServiceImplException {
        if(LANG == null || LANG.isEmpty()){
            throw new ResourceServiceImplException(LanguageNotSetException);
        }
        Resource res = resourceDao.findByKeyAndDict(LANG, key);
        if(res != null) return FromModelToDto(res);

        if(LANG2 == null || LANG2.isEmpty()){
            throw new ResourceServiceImplException(LanguageNotSetException);
        }
        res = resourceDao.findByKeyAndDict(LANG2, key);
        if(res != null) return FromModelToDto(res);

        return null;
    }

    @Override
    public String findValueByKey(String key) throws ResourceServiceImplException {
        if(LANG == null || LANG.isEmpty()){
            throw new ResourceServiceImplException(LanguageNotSetException);
        }
        Resource res = resourceDao.findByKeyAndDict(LANG, key);
        if(res != null) return res.getValue();

        if(LANG2 == null || LANG2.isEmpty()){
            throw new ResourceServiceImplException(LanguageNotSetException);
        }
        res = resourceDao.findByKeyAndDict(LANG2, key);
        if(res != null) return res.getValue();

        return "";
    }

    @Override
    public ResourceDto FromModelToDto(Resource model) {
        ResourceDto resourceDto = new ResourceDto();
        Mapper.Convert(model, resourceDto);
        return resourceDto;
    }

    @Override
    public Resource FromDtoToModel(ResourceDto dto) {
        Resource resource = new Resource();
        Mapper.Convert(dto, resource);
        return resource;
    }

    public class ResourceServiceImplException extends Exception
    {
        public ResourceServiceImplException(String message){
            super(message);
        }
    }
}
