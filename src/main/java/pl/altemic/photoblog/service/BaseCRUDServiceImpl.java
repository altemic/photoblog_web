package pl.altemic.photoblog.service;

import pl.altemic.photoblog.dao.BaseCRUD;

import javax.inject.Inject;
import java.io.Serializable;

/**
 * Created by michal on 18.09.2016.
 */

public abstract class BaseCRUDServiceImpl<T,V> implements BaseCRUDService<T>, Serializable {

    @Inject
    protected BaseCRUD<V> baseCRUD;

    public abstract T FromModelToDto(V model);
    public abstract V FromDtoToModel(T dto);

    @Override
    public void save(T dto){
        V model = FromDtoToModel(dto);
        baseCRUD.save(model);
    }

    @Override
    public T update(T dto){
        V model = FromDtoToModel(dto);
        V updated = baseCRUD.update(model);
        return FromModelToDto(updated);
    }

    @Override
    public void remove(Long id){
        baseCRUD.remove(id);
    }

    @Override
    public T get(Long id){
        V model = baseCRUD.get(id);
        T dto = FromModelToDto(model);
        return dto;
    }
}
