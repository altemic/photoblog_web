package pl.altemic.photoblog.service;

import pl.altemic.photoblog.dto.ResourceDto;

import java.util.List;

/**
 * Created by michal on 16.10.2016.
 */
public interface ResourceService extends BaseCRUDService<ResourceDto> {
    ResourceDto findByKeyAndDict(String dict, String key);
    List<ResourceDto> findAll();
    List<ResourceDto> findAllByDict(String dict);
    String findValueByKeyAndDict(String dict, String key);
    ResourceDto findByKey(String key) throws ResourceServiceImpl.ResourceServiceImplException;
    String findValueByKey(String key) throws ResourceServiceImpl.ResourceServiceImplException;
    void setLanguage(String language);
    List<String> findAllDicts();
}
