package pl.altemic.photoblog.service;

import pl.altemic.photoblog.dao.RecordDao;
import pl.altemic.photoblog.dto.PictureDto;
import pl.altemic.photoblog.dto.RecordDto;
import pl.altemic.photoblog.dto.UserDto;
import pl.altemic.photoblog.model.Picture;
import pl.altemic.photoblog.model.Record;
import pl.altemic.photoblog.model.User;

import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by michal on 21.08.2016.
 */
@SessionScoped
public class RecordServiceImpl extends BaseCRUDServiceImpl<RecordDto, Record> implements RecordService {

    private RecordDao recordDao;
    private BaseCRUDServiceImpl<PictureDto, Picture> pictureService;
    private BaseCRUDServiceImpl<UserDto, User> userService;

    @Inject
    public RecordServiceImpl(RecordDao _recordDao, BaseCRUDServiceImpl<PictureDto, Picture> _pictureService, BaseCRUDServiceImpl<UserDto, User> _userService){
        this.pictureService = _pictureService;
        this.recordDao = _recordDao;
        this.userService = _userService;
    }

    @Override
    public void save(RecordDto recordViewModel) {
        recordDao.save(FromDtoToModel(recordViewModel));
    }

    @Override
    public RecordDto update(RecordDto obj) {
        return FromModelToDto(recordDao.update(FromDtoToModel(obj)));
    }

    @Override
    public RecordDto get(Long id) {
        return FromModelToDto(recordDao.get(id));
    }

    @Override
    public List<RecordDto> getPagedRecords(int page, int count) {
        return recordDao.getPagedRrecords(page, count).stream().map(r -> FromModelToDto(r)).collect(Collectors.toList());
    }

    @Override
    public int countRecords(){
        return recordDao.countRecords();
    }

    @Override
    public void remove(Long id) {
        recordDao.remove(id);
    }

    public RecordDto FromModelToDto(Record record){
        RecordDto recordDto = new RecordDto();
        recordDto.setId(record.getId());
        recordDto.setTitle(record.getTitle());
        recordDto.setDate(record.getDate());
        recordDto.setDescription(record.getDescription());
        recordDto.setPicture(pictureService.FromModelToDto(record.getPicture()));
        recordDto.setUser(userService.FromModelToDto(record.getUser()));
        return recordDto;
    }

    public Record FromDtoToModel(RecordDto recordViewModel) {
        Record record = new Record();
        User user = userService.FromDtoToModel(recordViewModel.getUser());
        record.setUser(user);
        record.setId(recordViewModel.getId());
        record.setDate(recordViewModel.getDate());
        record.setTitle(recordViewModel.getTitle());
        record.setDescription(recordViewModel.getDescription());
        record.setPicture(pictureService.FromDtoToModel(recordViewModel.getPicture()));
        record.setId(recordViewModel.getId());
        return record;
    }
}
