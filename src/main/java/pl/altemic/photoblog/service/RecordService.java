package pl.altemic.photoblog.service;

import pl.altemic.photoblog.dto.RecordDto;

import java.util.List;

/**
 * Created by michal on 21.08.2016.
 */
public interface RecordService extends BaseCRUDService<RecordDto>{
    int countRecords();
    List<RecordDto> getPagedRecords(int page, int records);
}
