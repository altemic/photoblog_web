package pl.altemic.photoblog.service;

import pl.altemic.photoblog.dto.RoleDto;

import java.util.List;

/**
 * Created by michal on 28.08.2016.
 */
public interface RoleService extends BaseCRUDService<RoleDto>{
    List<RoleDto> findAll();
}
