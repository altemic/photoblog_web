package pl.altemic.photoblog.service;

import pl.altemic.photoblog.dao.RoleDao;
import pl.altemic.photoblog.dao.UserDao;
import pl.altemic.photoblog.dto.RoleDto;
import pl.altemic.photoblog.model.Role;

import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by michal on 28.08.2016.
 */
@SessionScoped
public class RoleServiceImpl extends BaseCRUDServiceImpl<RoleDto, Role> implements RoleService {

    private RoleDao roleDao;
    private UserDao userDao;

    @Inject
    public RoleServiceImpl(RoleDao _roleDao, UserDao _userDao){
        this.roleDao = _roleDao;
        this.userDao = _userDao;
    }

    @Override
    public List<RoleDto> findAll() {
        return roleDao.findAll().stream().map(r -> FromModelToDto(r)).collect(Collectors.toList());
    }

    @Override
    public void save(RoleDto roleViewModel) {
        Role role = FromDtoToModel(roleViewModel);
        roleDao.save(role);
    }

    @Override
    public void remove(Long id) {
        roleDao.remove(id);
    }

    @Override
    public RoleDto update(RoleDto obj) {
        Role role = FromDtoToModel(obj);
        return FromModelToDto(roleDao.update(role));
    }

    @Override
    public RoleDto get(Long id) {
        RoleDto roleDto = FromModelToDto(roleDao.get(id));
        return roleDto;
    }

    @Override
    public Role FromDtoToModel(RoleDto roleDto){
        Role role = new Role();
        role.setId(roleDto.getId());
        role.setTitle(roleDto.getTitle());
        role.setAccessLevel(roleDto.getAccessLevel());
        return role;
    }

    @Override
    public RoleDto FromModelToDto(Role role){
        RoleDto roleDto = new RoleDto();
        roleDto.setId(role.getId());
        roleDto.setAccessLevel(role.getAccessLevel());
        roleDto.setTitle(role.getTitle());
        return roleDto;
    }
}
