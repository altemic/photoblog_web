package pl.altemic.photoblog.service;

import pl.altemic.photoblog.dto.PictureDto;

/**
 * Created by michal on 29.08.2016.
 */
public interface PictureService extends BaseCRUDService<PictureDto>{
}
