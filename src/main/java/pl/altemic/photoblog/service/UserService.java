package pl.altemic.photoblog.service;

import pl.altemic.photoblog.dto.UserDto;

import java.util.List;

/**
 * Created by michal on 28.08.2016.
 */
public interface UserService extends BaseCRUDService<UserDto>{
    List<UserDto> findAll();
    UserDto findByName(String username);
}
