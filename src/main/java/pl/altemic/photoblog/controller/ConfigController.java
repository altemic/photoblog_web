package pl.altemic.photoblog.controller;

import pl.altemic.photoblog.util.ConfigScheme;
import pl.altemic.photoblog.util.ConfigService;

import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by michal on 16.10.2016.
 */
@WebServlet(name = "ConfigController", urlPatterns = {"/controlpanel", "/controlpanel/update"})
public class ConfigController extends HttpServlet {

    @Inject
    private ConfigService configService;

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String configFieldName = request.getParameter("name");
        if(configFieldName!=null && !configFieldName.isEmpty()){
            ConfigScheme cfgScheme = configService.load();
            String newValue = request.getParameter("value");
            try {
                cfgScheme.SetField(configFieldName, newValue);
            } catch (NoSuchFieldException ex){
                ex.printStackTrace();
            }
            configService.save(cfgScheme);
        }
        response.sendRedirect(getServletContext().getContextPath() + "/controlpanel");
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.getRequestDispatcher("/WEB-INF/ControlPanel.jsp").forward(request, response);
    }

    public static class PostACTION {
        public static final String UPDATE = "update";
    }
}
