package pl.altemic.photoblog.controller;

import pl.altemic.photoblog.dao.ResourceDao;
import pl.altemic.photoblog.dto.Mapper;
import pl.altemic.photoblog.dto.ResourceDto;
import pl.altemic.photoblog.service.ResourceService;
import pl.altemic.photoblog.util.ConfigScheme;
import pl.altemic.photoblog.util.ConfigService;
import pl.altemic.photoblog.viewmodel.ResourceViewModel;

import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by michal on 18.10.2016.
 */
@WebServlet(name = "ResourceController", urlPatterns = {"/resources", "/resources/update", "/resources/new"})
public class ResourceController extends HttpServlet {

    @Inject
    private ResourceService resourceService;

    @Inject
    private ResourceDao resourceDao;

    @Inject
    private ConfigService configService;

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        RequestHelper requestHelper = new RequestHelper(request);
        String action = requestHelper.getMethod();

        switch (action){
            case PostACTION.UPDATE:
                ResourceViewModel resourceViewModel = ExtractFromRequest(request);
                ResourceDto resourceDto = ConvertFromViewModelToDto(resourceViewModel);
                resourceService.update(resourceDto);
                break;
            case PostACTION.NEW:
                ConfigScheme cfgScheme = configService.load();
                String lang = cfgScheme.getDefLanguage();
                String newLang = request.getParameter("newLanguage");
                List<ResourceDto> allResourcesList = resourceService.findAllByDict(lang.toUpperCase());
                for(ResourceDto res : allResourcesList){
                    ResourceDto newResource = new ResourceDto();
                    newResource.setDict(newLang);
                    newResource.setValue(res.getValue());
                    newResource.setKey(res.getKey());
                    resourceService.save(newResource);
                }
                break;
            default:
                break;
        }

        response.sendRedirect(getServletContext().getContextPath() + "/resources");
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String lang = request.getParameter("lang");
        if(lang==null) lang = configService.load().getLanguage();
        List<ResourceViewModel> allResourcesList;
        if(lang == null || lang.isEmpty() || !resourceService.findAllDicts().contains(lang)) {
            allResourcesList = resourceService.findAll().stream().map(r -> ConvertFromDtoToViewModel(r)).collect(Collectors.toList());
        } else {
            allResourcesList = resourceService.findAllByDict(lang.toUpperCase()).stream().map(r -> ConvertFromDtoToViewModel(r)).collect(Collectors.toList());
        }

        request.setAttribute("resourcesList", allResourcesList);
        List<String> allDicts = resourceService.findAllDicts();
        request.setAttribute("dictionaryList", allDicts);
        request.getRequestDispatcher("/WEB-INF/Resources.jsp").forward(request, response);
    }

    public ResourceViewModel ExtractFromRequest(HttpServletRequest request){
        ResourceViewModel resourceViewModel = new ResourceViewModel();
        resourceViewModel.setId(Long.valueOf(request.getParameter("id")));
        resourceViewModel.setDict(request.getParameter("dict"));
        resourceViewModel.setKey(request.getParameter("key"));
        resourceViewModel.setValue(request.getParameter("value"));
        return resourceViewModel;
    }

    public static class PostACTION {
        public static final String UPDATE = "update";
        public static final String NEW = "new";
    }

    public static ResourceDto ConvertFromViewModelToDto(ResourceViewModel resourceViewModel) {
        ResourceDto resourceDto = new ResourceDto();
        Mapper.Convert(resourceViewModel, resourceDto);
        return resourceDto;
    }

    public static ResourceViewModel ConvertFromDtoToViewModel(ResourceDto resourceDto){
        ResourceViewModel resourceViewModel = new ResourceViewModel();
        Mapper.Convert(resourceDto, resourceViewModel);
        return resourceViewModel;
    }
}
