package pl.altemic.photoblog.controller;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by michal on 02.10.2016.
 */
public class RequestHelper {
    private HttpServletRequest request;

    public RequestHelper(HttpServletRequest request){
        this.request = request;
    }

    public String getMethod(){
        String route = request.getServletPath();
        String[] params = route.split("/");
        if(params.length == 2) return "";
        String method = params[params.length-1];
        return method;
    }

    public void getUser(){

    }
}
