package pl.altemic.photoblog.controller;

import pl.altemic.photoblog.dto.Mapper;
import pl.altemic.photoblog.dto.RoleDto;
import pl.altemic.photoblog.dto.UserDto;
import pl.altemic.photoblog.service.RoleService;
import pl.altemic.photoblog.service.UserService;
import pl.altemic.photoblog.viewmodel.RoleViewModel;
import pl.altemic.photoblog.viewmodel.UserViewModel;

import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by michal on 03.08.2016.
 */
@WebServlet(name = "UserController", urlPatterns = {"/users", "/users/add", "/users/new", "/users/edit", "/users/update", "/users/remove"})
public class UserController extends HttpServlet {

    @Inject
    private UserService userService;

    @Inject
    private RoleService roleService;

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        RequestHelper requestHelper = new RequestHelper(request);
        String action = requestHelper.getMethod();

        UserViewModel userViewModel = extractUser(request);
        UserDto userDto = ConvertFromViewModelToDto(userViewModel);

        switch (action) {
            case PostACTION.ADD:
                userDto.setId(0);
                userService.save(userDto);
                break;
            case PostACTION.UPDATE:
                userService.update(userDto);
                break;
            case PostACTION.REMOVE:
                userService.remove(userDto.getId());
                break;
        }

        response.sendRedirect(getServletContext().getContextPath() + "/users");
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        RequestHelper requestHelper = new RequestHelper(request);
        String action = requestHelper.getMethod();

        switch (action) {
            case GetACTION.EDIT:
                String id = request.getParameter("id");
                if(id !=null && !id.isEmpty()) {
                    UserViewModel userViewModel = ConvertFromDtoToViewModel(userService.get(Long.valueOf(id)));
                    request.setAttribute("user", userViewModel);
                    List<RoleViewModel> roleList = roleService.findAll().stream().map(r -> ConvertFromDtoToViewModel(r)).collect(Collectors.toList());
                    request.setAttribute("roleList", roleList);
                    request.getRequestDispatcher("/WEB-INF/User.jsp").forward(request, response);
                } else {
                    request.getRequestDispatcher("/WEB-INF/Users.jsp").forward(request, response);
                }
                break;
            case GetACTION.NEW:
                List<RoleViewModel> roleList = roleService.findAll().stream().map(r -> ConvertFromDtoToViewModel(r)).collect(Collectors.toList());
                request.setAttribute("roleList", roleList);
                request.getRequestDispatcher("/WEB-INF/User.jsp").forward(request, response);
                break;
            default:
                List<UserViewModel> userList = userService.findAll().stream().map(u -> ConvertFromDtoToViewModel(u)).collect(Collectors.toList());
                request.setAttribute("userList", userList);
                request.getRequestDispatcher("/WEB-INF/Users.jsp").forward(request, response);
                break;
        }
    }

    public static class PostACTION {
        public static final String UPDATE = "update";
        public static final String REMOVE = "remove";
        public static final String ADD = "add";
    }

    public static class GetACTION{
        public static final String EDIT = "edit";
        public static final String NEW = "new";
    }

    public UserViewModel extractUser(HttpServletRequest request){
        UserViewModel user = new UserViewModel();
        String id = request.getParameter("id");
        if(id !=null && !id.isEmpty()) {
            user.setId(Long.valueOf(id));
        }
        user.setUsername(request.getParameter("username"));
        user.setFirstname(request.getParameter("firstname"));
        user.setLastname(request.getParameter("lastname"));
        user.setPassword(request.getParameter("password"));

        List<RoleViewModel> roleList = roleService.findAll().stream().map(r -> UserController.ConvertFromDtoToViewModel(r)).collect(Collectors.toList());
        for(RoleViewModel r : roleList) {
            String parameter = request.getParameter(r.getTitle());
            if(parameter!=null && parameter.equals("on")) {
                RoleViewModel role = (RoleViewModel) roleList.stream().filter(rx -> rx.getTitle().equals(r.getTitle())).toArray()[0];
                user.getRoles().add(role);
            }
        }
        return user;
    }

    public static UserDto ConvertFromViewModelToDto(UserViewModel userViewModel){
        UserDto userDto = new UserDto();
        userDto.setId(userViewModel.getId());
//        userDto.setDevices();
        userDto.setEmail(userViewModel.getEmail());
        userDto.setFirstname(userViewModel.getFirstname());
        userDto.setLastname(userViewModel.getLastname());
        userDto.setPassword(userViewModel.getPassword());
        List<RoleDto> roleDtoList = userViewModel.getRoles().stream().map(r -> ConvertFromViewModelToDto(r)).collect(Collectors.toList());
        userDto.setRoles(roleDtoList);
        userDto.setUsername(userViewModel.getUsername());
        return userDto;
    }

    public static UserViewModel ConvertFromDtoToViewModel(UserDto userDto){
        UserViewModel userViewModel = new UserViewModel();
        userViewModel.setId(userDto.getId());
        userViewModel.setPassword(userDto.getPassword());
        userViewModel.setUsername(userDto.getUsername());
        userViewModel.setFirstname(userDto.getFirstname());
        userViewModel.setLastname(userDto.getLastname());
        userViewModel.setEmail(userDto.getEmail());
//        userViewModel.setDevices();
        List<RoleViewModel> roleVMList = userDto.getRoles().stream().map(r -> ConvertFromDtoToViewModel(r)).collect(Collectors.toList());
        userViewModel.setRoles(roleVMList);
        return userViewModel;
    }

    public static RoleDto ConvertFromViewModelToDto(RoleViewModel role){
        RoleDto roleDto = new RoleDto();
        Mapper.Convert(role, roleDto);
        return roleDto;
    }

    public static RoleViewModel ConvertFromDtoToViewModel(RoleDto roleDto){
        RoleViewModel roleViewModel = new RoleViewModel();
        Mapper.Convert(roleDto, roleViewModel);
        return roleViewModel;
    }
}
