package pl.altemic.photoblog.controller;

import pl.altemic.photoblog.dao.PictureDao;
import pl.altemic.photoblog.dto.PictureDto;
import pl.altemic.photoblog.model.Picture;
import pl.altemic.photoblog.viewmodel.PictureViewModel;

import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by michal on 09.08.2016.
 */
@WebServlet(name = "PictureController", urlPatterns = "/Pictures")
public class PictureController extends HttpServlet {

    @Inject
    private PictureDao pictureDao;

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String pictureId = request.getParameter("id");
        Picture picture = pictureDao.get(Long.parseLong(pictureId));
        response.setContentType("image/jpeg, image/jpg, image/png, image/gif");
        response.getOutputStream().write(picture.getPicture());
        response.getOutputStream().close();
    }

    public static PictureDto ConvertFromViewModelToDto(PictureViewModel pictureViewModel){
        PictureDto pictureDto = new PictureDto();
        pictureDto.setId(pictureViewModel.getId());
        pictureDto.setPicture(pictureViewModel.getPicture());
        pictureDto.setExtension(pictureViewModel.getExtension());
//        Inifnite loop
//        pictureDto.setRecord(pictureViewModel.getRecord());
        return pictureDto;
    }

    public static PictureViewModel ConvertFromDtoToViewModel(PictureDto pictureDto){
        PictureViewModel pictureViewModel = new PictureViewModel();
        pictureViewModel.setId(pictureDto.getId());
        pictureViewModel.setPicture(pictureDto.getPicture());
        pictureViewModel.setExtension(pictureDto.getExtension());
        return pictureViewModel;
    }
}
