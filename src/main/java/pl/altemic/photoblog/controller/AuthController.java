package pl.altemic.photoblog.controller;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by michal on 08.10.2016.
 */
@WebServlet(name = "AuthController", urlPatterns = {"/auth/logout", "/auth/login"})
public class AuthController extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.sendRedirect(getServletContext().getContextPath() + "/");
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        RequestHelper requestHelper = new RequestHelper(request);
        String action = requestHelper.getMethod();

        switch (action){
            case GetACTION.LOGOUT:
                request.logout();
                request.getSession(false).invalidate();
                response.setHeader("Cache-Control", "no-cache, no-store");
                response.setHeader("Pragma", "no-cache");
                break;
            case GetACTION.LOGIN:
                request.getSession(false).invalidate();
                response.setHeader("Cache-Control", "no-cache, no-store");
                response.setHeader("Pragma", "no-cache");
                break;
        }

        response.sendRedirect(getServletContext().getContextPath() + "/");
    }

    public static class GetACTION {
        public static final String LOGOUT = "logout";
        public static final String LOGIN = "login";
    }
}
