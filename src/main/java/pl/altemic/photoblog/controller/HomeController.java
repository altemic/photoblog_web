package pl.altemic.photoblog.controller;

import pl.altemic.photoblog.dto.ResourceDto;
import pl.altemic.photoblog.service.RecordService;
import pl.altemic.photoblog.service.ResourceService;
import pl.altemic.photoblog.util.PageHelper;
import pl.altemic.photoblog.viewmodel.RecordViewModel;

import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by michal on 30.07.2016.
 */
@WebServlet(name = "HomeController")
public class HomeController extends HttpServlet {

    private final String recordsPerPageCookie_NAME = "recordsPerPage";
    private final String recordsPageNoCookie_NAME = "pageNo";

    @Inject
    private RecordService recordService;

    @Inject
    private PageHelper pageHelper;


    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.sendRedirect(getServletContext().getContextPath() + "/");
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        HttpSession session = request.getSession(true);
        refreshHelper(request, response);
        List<RecordViewModel> recordList
                = recordService.getPagedRecords(pageHelper.getPage(), pageHelper.getRecordsPerPage()).stream()
                .map(r -> RecordController.ConvertFromDtoToViewModel(r)).collect(Collectors.toList());
        session.setAttribute("recordList", recordList);
        request.getRequestDispatcher("/WEB-INF/index.jsp").forward(request, response);
    }

    private void refreshHelper(HttpServletRequest request, HttpServletResponse response) {
        String pageNoSt = request.getParameter("page");
        if(pageNoSt == null || Integer.valueOf(pageNoSt) <= 1) {
            pageHelper.setPage(1);
        } else {
            int recordsNo = recordService.countRecords();
            int maxPageNo = recordsNo/pageHelper.getRecordsPerPage() + 1;
            int selectedPage = Integer.valueOf(pageNoSt);
            if(selectedPage > maxPageNo){
                selectedPage = maxPageNo;
            }
            pageHelper.setPage(selectedPage);
        }

        HttpSession session = request.getSession();
        session.setAttribute("pageNo", pageHelper.getPage());

//        Cookie[] cookies = request.getCookies();

//        if(cookies != null) {
//            for (Cookie c : cookies) {
//                if (c.getName().equals(recordsPerPageCookie_NAME)) {
//                    int cookieValue = Integer.valueOf(c.getValue());
//                    pageHelper.setRecordsPerPage(cookieValue);
//                } else {
//                    String recordsPerPageSt = String.valueOf(pageHelper.getRecordsPerPage());
//                    Cookie recordsPerPageCookie = new Cookie(recordsPerPageCookie_NAME, recordsPerPageSt);
//                    recordsPerPageCookie.setMaxAge(Integer.MAX_VALUE);
//                    response.addCookie(recordsPerPageCookie);
//                }
//
//                if (c.getName().equals(recordsPageNoCookie_NAME)) {
//                    int cookieValue = Integer.valueOf(c.getValue());
//                    pageHelper.setPage(cookieValue);
//                } else {
//                    Cookie pageNoCookie = new Cookie(recordsPerPageCookie_NAME, "1");
//                    pageNoCookie.setMaxAge(Integer.MAX_VALUE);
//                    response.addCookie(pageNoCookie);
//                }
//            }
//        }
    }
}
