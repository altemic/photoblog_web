package pl.altemic.photoblog.controller;

import com.sun.deploy.net.HttpResponse;
import pl.altemic.photoblog.dto.PictureDto;
import pl.altemic.photoblog.dto.RecordDto;
import pl.altemic.photoblog.service.RecordService;
import pl.altemic.photoblog.service.UserService;
import pl.altemic.photoblog.util.PictureUtilities;
import pl.altemic.photoblog.viewmodel.PictureViewModel;
import pl.altemic.photoblog.viewmodel.RecordViewModel;
import pl.altemic.photoblog.viewmodel.UserViewModel;

import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Timestamp;

/**
 * Created by michal on 03.08.2016.
 */
@WebServlet(urlPatterns = {"/records", "/records/add", "/records/remove", "/records/edit", "/records/update"})
@MultipartConfig
public class RecordController extends HttpServlet {

    private RecordService recordService;
    private UserService userService;
    private PictureUtilities pictureUtilities;

    @Inject
    public RecordController(RecordService _recordService, UserService _userService, PictureUtilities _pictureUtilities) {
        this.recordService = _recordService;
        this.userService = _userService;
        this.pictureUtilities = _pictureUtilities;
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        RequestHelper requestHelper = new RequestHelper(request);
        String action = requestHelper.getMethod();

        switch (action) {
            case PostACTION.ADD:
                String username = request.getUserPrincipal().getName();
                UserViewModel userViewModel = UserController.ConvertFromDtoToViewModel(userService.findByName(username));
                RecordViewModel record = RetrieveFromRequest(request);
                if(record == null){
                    response.sendRedirect(getServletContext().getContextPath() + "/");
                    break;
                }
                record.setUser(userViewModel);
                recordService.save(ConvertFromViewModelToDto(record));
                break;
            case PostACTION.REMOVE:
                String idSt = request.getParameter("id");
                if(idSt!=null) {
                    long id = Long.valueOf(idSt);
                    recordService.remove(id);
                }
                break;
            case PostACTION.UPDATE:
                String username2 = request.getUserPrincipal().getName();
                UserViewModel userViewModel2 = UserController.ConvertFromDtoToViewModel(userService.findByName(username2));
                RecordViewModel record2 = RetrieveFromRequest(request);
                if(record2 == null){
                    response.sendRedirect(getServletContext().getContextPath() + "/");
                    break;
                }
                record2.setUser(userViewModel2);
                recordService.update(ConvertFromViewModelToDto(record2));
                break;
            case GetACTION.EDIT:
                //TODO: in GET method from browser parameters are null in servlet, when requset is send by other program, everything is correct - strange
                //temporary obey
                doGet(request, response);
                break;
        }

        response.sendRedirect(getServletContext().getContextPath() + "/");
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        RequestHelper requestHelper = new RequestHelper(request);
        String action = requestHelper.getMethod();

        switch (action){
            case GetACTION.NEW:
                request.getRequestDispatcher("/WEB-INF/AddPhoto.jsp").forward(request, response);
                break;
            case GetACTION.EDIT:
                String idSt = request.getParameter("id");
                if(idSt!=null) {
                    RecordViewModel record = RecordController.ConvertFromDtoToViewModel(recordService.get(Long.valueOf(idSt)));
                    request.setAttribute("record", record);
                    request.getRequestDispatcher("/WEB-INF/AddPhoto.jsp").forward(request, response);
                } else {
                    response.sendRedirect(getServletContext().getContextPath() + "/");
                }
                break;
            default:
                request.getRequestDispatcher("/WEB-INF/AddPhoto.jsp").forward(request, response);
                break;
        }
    }


    public static class GetACTION{
        public static final String NEW = "new";
        public static final String EDIT = "edit";
    }

    public static class PostACTION{
        public static final String ADD = "add";
        public static final String REMOVE = "remove";
        public static final String UPDATE = "update";
    }

    private RecordViewModel RetrieveFromRequest(HttpServletRequest request) throws ServletException, IOException {

        Part filePart = request.getPart("image");
        InputStream fileContent = filePart.getInputStream();
        byte[] tempArr = new byte[fileContent.available()];
        RecordViewModel record = new RecordViewModel();
        PictureViewModel picture = new PictureViewModel();
        String angleStr = request.getParameter("angle");
        String idSt = request.getParameter("id");
        if(tempArr.length != 0) {
            if(idSt != null){
                record.setId(Long.valueOf(idSt));
            }
            String contentType = filePart.getContentType();
            String extension = contentType.split("/")[1];
            fileContent.read(tempArr);
            if (angleStr != null) {
                int angle = Integer.valueOf(angleStr);
                tempArr = pictureUtilities.rotate(tempArr, angle, extension);
            }
            picture.setExtension(extension);
            picture.setPicture(tempArr);
            picture.setRecord(record);
            record.setPicture(picture);
        } else if(idSt != null) {
            record.setId(Long.valueOf(idSt));
            picture = PictureController.ConvertFromDtoToViewModel(recordService.get(Long.valueOf(idSt)).getPicture());
            tempArr  = picture.getPicture();
            if (angleStr != null) {
                int angle = Integer.valueOf(angleStr);
                tempArr = pictureUtilities.rotate(tempArr, angle, picture.getExtension());
            }

            picture.setPicture(tempArr);
            picture.setRecord(record);
            record.setPicture(picture);
        } else {
            return null;
        }

        record.setDate(new Timestamp(System.currentTimeMillis()));
        record.setDescription(request.getParameter("description"));
        record.setTitle(request.getParameter("title"));

        return record;
    }

    public static RecordDto ConvertFromViewModelToDto(RecordViewModel recordViewModel){
        RecordDto recordDto = new RecordDto();
        PictureDto pictureDto = PictureController.ConvertFromViewModelToDto(recordViewModel.getPicture());
        recordDto.setId(recordViewModel.getId());
        recordDto.setDate(recordViewModel.getDate());
        recordDto.setDescription(recordViewModel.getDescription());
        recordDto.setPicture(pictureDto);
        recordDto.setTitle(recordViewModel.getTitle());
        recordDto.setUser(UserController.ConvertFromViewModelToDto(recordViewModel.getUser()));
        return recordDto;
    }

    public static RecordViewModel ConvertFromDtoToViewModel(RecordDto recordDto){
        RecordViewModel recordViewModel = new RecordViewModel();
        recordViewModel.setId(recordDto.getId());
        recordViewModel.setTitle(recordDto.getTitle());
        recordViewModel.setDescription(recordDto.getDescription());
        recordViewModel.setUser(UserController.ConvertFromDtoToViewModel(recordDto.getUser()));
        recordViewModel.setPicture(PictureController.ConvertFromDtoToViewModel(recordDto.getPicture()));
        recordViewModel.setDate(recordDto.getDate());
        return recordViewModel;
    }
}



