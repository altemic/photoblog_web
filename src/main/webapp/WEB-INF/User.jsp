<%--
  Created by IntelliJ IDEA.
  User: michal
  Date: 03.08.2016
  Time: 20:06
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page import="pl.altemic.photoblog.viewmodel.UserViewModel" %>
<%@ page import="pl.altemic.photoblog.viewmodel.RoleViewModel" %>
<html>
<head>
    <title>Add user</title>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
    <meta name="viewport" content="width=device-width; initial-scale=1.0"/>
    <link href="${pageContext.request.contextPath}/theme/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <%--<link href="${pageContext.request.contextPath}/css/piesekfiona_custom.css" type="text/css" rel="stylesheet">--%>
</head>
<body>

    <jsp:include page="fragments/header.jsp"></jsp:include>

    <div class="container">
        <div class="row">
            <div class="col-md-8">
                <c:choose>
                    <c:when test="${requestScope.user.getId() != null}">
                        <h1>
                            ${sessionScope.resourceService.findValueByKey("Modify user")}
                        </h1>
                    </c:when>
                    <c:otherwise>
                        <h1>
                            ${sessionScope.resourceService.findValueByKey("Add user")}
                        </h1>
                    </c:otherwise>
                </c:choose>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <c:choose>
                    <c:when test="${requestScope.user.getId() != null}">
                        <c:set var="user_id" value="${requestScope.user.getId()}"></c:set>
                        <c:set var="method" value="update"></c:set>
                    </c:when>
                    <c:otherwise>
                        <c:set var="user_id" value="-1"></c:set>
                        <c:set var="method" value="add"></c:set>
                    </c:otherwise>
                </c:choose>

                <form action="${pageContext.request.contextPath}/users/<c:out value="${method}"></c:out>" method="post">
                    <div class="form-group">
                        <input type="hidden" name="id" value="<c:out value="${user_id}"></c:out>">

                        <label for="NickNameInput">
                            ${sessionScope.resourceService.findValueByKey("Username")}
                        </label>
                        <c:choose>
                            <c:when test="${requestScope.user.getUsername() != null && !requestScope.user.getUsername().isEmpty()}">
                                <input type="text" name="username" value="<c:out value="${requestScope.user.getUsername()}"></c:out>" id="NickNameInput" class="form-control">
                            </c:when>
                            <c:otherwise>
                                <input type="text" name="username" placeholder="${sessionScope.resourceService.findValueByKey("Username")}" id="NickNameInput" class="form-control">
                            </c:otherwise>
                        </c:choose>
                    </div>
                    <div class="form-group">
                        <label for="FirstNameInput">
                            ${sessionScope.resourceService.findValueByKey("First name")}
                        </label>
                        <c:choose>
                            <c:when test="${requestScope.user.getFirstname() != null && !requestScope.user.getFirstname().isEmpty()}">
                                <input type="text" name="firstname" value="<c:out value="${requestScope.user.getFirstname()}"></c:out>" id="FirstNameInput" class="form-control">
                            </c:when>
                            <c:otherwise>
                                <input type="text" name="firstname" placeholder="${sessionScope.resourceService.findValueByKey("First name")}" id="FirstNameInput" class="form-control">
                            </c:otherwise>
                        </c:choose>
                    </div>
                    <div class="form-group">
                        <label for="LastNameInput">
                            ${sessionScope.resourceService.findValueByKey("Last name")}
                        </label>
                        <c:choose>
                            <c:when test="${requestScope.user.getLastname() != null && !requestScope.user.getLastname().isEmpty()}">
                                <input type="text" name="lastname" value="<c:out value="${requestScope.user.getLastname()}"></c:out>" id="LastNameInput" class="form-control">
                            </c:when>
                            <c:otherwise>
                                <input type="text" name="lastname" placeholder="${sessionScope.resourceService.findValueByKey("Last name")}" id="LastNameInput" class="form-control">
                            </c:otherwise>
                        </c:choose>
                    </div>
                    <div class="form-group">
                        <label for="PasswordInput">
                            ${sessionScope.resourceService.findValueByKey("Password")}
                        </label>
                        <c:choose>
                            <c:when test="${requestScope.user.getPassword() != null && !requestScope.user.getPassword().isEmpty()}">
                                <input type="password" name="password" value="<c:out value="${requestScope.user.getPassword()}"></c:out>" id="PasswordInput" class="form-control">
                            </c:when>
                            <c:otherwise>
                                <input type="password" name="password" placeholder="${sessionScope.resourceService.findValueByKey("Password")}" id="PasswordInput" class="form-control">
                            </c:otherwise>
                        </c:choose>
                    </div>

                    <c:forEach var="role" items="${requestScope.roleList}">
                        <div class="checkbox">
                            <label>
                                <c:set var="contains" value="false" />
                                <c:forEach var="userRole" items="${user.getRoles()}">
                                    <c:if test="${userRole.getId() eq role.getId()}">
                                        <c:set var="contains" value="true" />
                                    </c:if>
                                </c:forEach>
                                <input type="checkbox" <c:if test="${contains}">checked="checked"</c:if>name="${role.getTitle()}"><c:out value="${role.getTitle()}"/>
                            </label>
                        </div>
                    </c:forEach>
                    <button type="submit" class="btn btn-default">
                        ${sessionScope.resourceService.findValueByKey("Add")} / ${sessionScope.resourceService.findValueByKey("Edit")}</button>
                </form>
            </div>
            <div class="col-md-4"></div>
        </div>
    </div>

    <script src="http://code.jquery.com/jquery-1.11.2.min.js"></script>
    <script src="http://code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
    <script src="${pageContext.request.contextPath}/resources/js/bootstrap.js"></script>

</body>
</html>
