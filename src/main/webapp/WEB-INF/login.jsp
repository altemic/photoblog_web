<%--
  Created by IntelliJ IDEA.
  User: michal
  Date: 12.10.2016
  Time: 19:36
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
    <title>piesekfiona.pl sign in!</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="login page">
    <meta name="author" content="altemic">
    <link href="${pageContext.request.contextPath}/theme/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/css/signin.css" rel="stylesheet">
</head>
<body>
    <div class="container">
        <form class="form-signin" action="j_security_check" method="post">
            <h2 class="form-signin-heading">
                ${sessionScope.resourceService.findValueByKey("Please sign in")}
            </h2>
            <label class="sr-only" for="usernameInput">
                ${sessionScope.resourceService.findValueByKey("Username")}
            </label>
            <input class="form-control" id="usernameInput" type="text" name="j_username">
            <label class="sr-only" for="passwordInput">
                ${sessionScope.resourceService.findValueByKey("Password")}
            </label>
            <input class="form-control" id="passwordInput" type="password" name="j_password">
            <input class="btn btn-lg btn-primary btn-block" type="submit" value="${sessionScope.resourceService.findValueByKey("Sign in")}">
        </form>
    </div>
</body>
</html>
