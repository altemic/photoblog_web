<%--
  Created by IntelliJ IDEA.
  User: michal
  Date: 03.09.2016
  Time: 14:39
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page import="pl.altemic.photoblog.viewmodel.UserViewModel" %>
<%@ page import="pl.altemic.photoblog.viewmodel.RoleViewModel" %>
<html>
    <head>
        <title>Users</title>
        <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
        <meta name="viewport" content="width=device-width; initial-scale=1.0"/>
        <link href="${pageContext.request.contextPath}/theme/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <link href="${pageContext.request.contextPath}/css/piesekfiona_custom.css" type="text/css" rel="stylesheet">

        <style>
            .modifyButton{
                width: 100%;
            }

            .longColumn{
                width: 20%;
            }

            .mediumColumn{
                width: 8%;
            }

            .shortColumn{
                width: 4%;
            }
        </style>

    </head>
    <body>
        <jsp:include page="fragments/header.jsp"></jsp:include>

        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h1>
                        ${sessionScope.resourceService.findValueByKey("User list")}
                    </h1>
                </div>
            </div>
            <div class="row">
                <div class="col-md-2">
                    <form method="get">
                        <button formaction="${pageContext.request.contextPath}/users/new" class="btn btn-primary">
                            ${sessionScope.resourceService.findValueByKey("New")}
                        </button>
                    </form>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div>
                        <table class="table table-bordered table-hover">
                            <thead>
                                <th class="shortColumn">#</th>
                                <th class="longColumn">${sessionScope.resourceService.findValueByKey("Username")}</th>
                                <th class="longColumn">${sessionScope.resourceService.findValueByKey("First name")}</th>
                                <th class="longColumn">${sessionScope.resourceService.findValueByKey("Last name")}</th>
                                <th class="longColumn">${sessionScope.resourceService.findValueByKey("Roles")}</th>
                                <th class="mediumColumn">${sessionScope.resourceService.findValueByKey("Action")}</th>
                                <th class="mediumColumn">${sessionScope.resourceService.findValueByKey("Action")}</th>
                            </thead>
                            <tbody>
                                <% int lp = 0; %>
                                <tr>
                                    <c:forEach var="user" items="${requestScope.userList}">
                                        <% lp++; %>
                                        <tr>
                                            <form>
                                                <input type="hidden" value="${user.getId()}" name="id">
                                                <td><%= lp %></td>
                                                <td>${user.getUsername()}</td>
                                                <td>${user.getFirstname()}</td>
                                                <td>${user.getLastname()}</td>
                                                <td>${user.getRolesString()}</td>
                                                <td>
                                                    <button
                                                            class="btn btn-primary modifyButton"
                                                            formmethod="get"
                                                            formaction="${pageContext.request.contextPath}/users/edit?id=${user.getId()}" >
                                                        ${sessionScope.resourceService.findValueByKey("Edit")}
                                                    </button>
                                                </td>
                                                <td>
                                                    <button class="btn btn-danger modifyButton"
                                                            formmethod="post"
                                                            formaction="${pageContext.request.contextPath}/users/remove?id=${user.getId()}">
                                                        ${sessionScope.resourceService.findValueByKey("Delete")}
                                                    </button>
                                                </td>
                                            </form>
                                        </tr>
                                    </c:forEach>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    <script type="application/javascript" src="${pageContext.request.contextPath}/js/AddPhoto.js"></script>
    </body>
</html>
