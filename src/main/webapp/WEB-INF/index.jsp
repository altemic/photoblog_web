<%@ page import="pl.altemic.photoblog.viewmodel.RecordViewModel" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<!DOCTYPE HTML>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="blog about dog whose name is fiona">
    <meta name="author" content="altemic">

    <title>Dog Blog</title>

    <!-- Bootstrap Core CSS -->
    <link href="${pageContext.request.contextPath}/theme/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css">

    <!-- Theme CSS -->
    <link href="${pageContext.request.contextPath}/theme/css/clean-blog.min.css" rel="stylesheet" type="text/css">

    <!-- Custom Fonts -->
    <link href="${pageContext.request.contextPath}/theme/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Lora:400,700,400italic,700italic" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <!--<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js" type="application/javascript"></script>-->
    <!--<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js" type="application/javascript"></script>-->

    <![endif]-->

    <style>
        .pagingButtons{
            width: 120px;
            display: inline;
        }
        .pageNo{
            padding-left: 30px;
            padding-right: 30px;
            display: inline;
        }
    </style>

</head>

<body>

<!-- Navigation -->
<nav class="navbar navbar-default navbar-custom navbar-fixed-top">
    <div class="container-fluid">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header page-scroll">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                <span class="sr-only">Toggle navigation</span>
                Menu <i class="fa fa-bars"></i>
            </button>
            <a class="navbar-brand" href="${pageContext.request.contextPath}/">
                ${sessionScope.Config.getPageName()}
            </a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav navbar-right">
                <li>
                    <c:choose>
                        <c:when test="${not empty pageContext.request.userPrincipal}">
                            <a href="${pageContext.request.contextPath}/auth/logout">
                                ${sessionScope.resourceService.findValueByKey("Logout")}
                            </a>
                        </c:when>
                        <c:otherwise>
                            <a href="${pageContext.request.contextPath}/auth/login">
                                ${sessionScope.resourceService.findValueByKey("Login")}
                            </a>
                        </c:otherwise>
                    </c:choose>
                </li>
                <li>
                    <a href="${pageContext.request.contextPath}/controlpanel">
                        ${sessionScope.resourceService.findValueByKey("Control panel")}
                    </a>
                </li>
                <li>
                    <a href="${pageContext.request.contextPath}/records/add">
                        ${sessionScope.resourceService.findValueByKey("Add record")}
                    </a>
                </li>
            </ul>
        </div>
        <!-- /.navbar-collapse -->
    </div>
    <!-- /.container -->
</nav>

<!-- Page Header -->
<!-- Set your background image for this header on the line below. -->
<header class="intro-header" style="background-image: url('${pageContext.request.contextPath}/theme/img/home-bg.jpg')">
    <div class="container">
        <div class="row">
            <div class="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1">
                <div class="site-heading">
                    <h1>Dog Blog</h1>
                    <hr class="small">
                    <span class="subheading">czyli piesek fiona prezentuje</span>
                </div>
            </div>
        </div>
    </div>
</header>

<!-- Main Content -->
<div class="container">
    <c:forEach var="record" items="${sessionScope.recordList}">
        <div class="row">
            <div class="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1">
                <div class="post-preview">
                    <h2 class="post-title">
                        <c:out value="${record.getTitle()}"></c:out>
                    </h2>
                    <img src="${pageContext.request.contextPath}/Pictures?id=${record.getPicture().getId()}" onload="scaleImg()" style="width: 700px"/>
                    <h3 class="post-subtitle">
                        <c:out value="${record.getDescription()}"></c:out>
                    </h3>
                    <p class="post-meta">Posted by <a href="#"><c:out value="${record.getUser().getUsername()}"></c:out></a> on <c:out value="${record.getDateString()}"></c:out></p>
                </div>
                <hr>
            </div>
            <!-- ONLY AUTHENTICATED USER -->
                <c:if test="${not empty pageContext.request.userPrincipal}">
                    <div class="col-lg-2 col-md-2 col-lg-offset-0">
                        <form>
                            <button
                                    formaction="${pageContext.request.contextPath}/records/remove?id=${record.getId()}"
                                    formmethod="post"
                                    type="submit"
                                    class="btn btn-default btn-sm pagingButtons">
                                <span><div class="glyphicon glyphicon-remove"></div>&nbsp;&nbsp;
                                        ${sessionScope.resourceService.findValueByKey("Delete")}
                                </span>
                            </button>
                            <div style="height: 30px;"></div>
                            <button
                                    formaction="${pageContext.request.contextPath}/records/edit?id=${record.getId()}"
                                    formmethod="post"
                                    type="submit"
                                    class="btn btn-default btn-sm pagingButton
                                    20.s">
                                <span>
                                    ${sessionScope.resourceService.findValueByKey("Edit")}
                                </span>
                            </button>
                        </form>
                    </div>
                </c:if>
        </div>
    </c:forEach>
    <div class="row">
        <ul class="pager">
            <li class="next">
                <form action="${pageContext.request.contextPath}/mainpage" method="get">
                    <fmt:parseNumber var="pageNoInt" type="number" value="${sessionScope.pageNo}" parseLocale="en_GB"/>
                        <button
                                name="page"
                                type="submit"
                                class="btn btn-default btn-sm pagingButtons"
                                value="${pageNoInt - 1}"
                                type="submit">
                            ${sessionScope.resourceService.findValueByKey("Previous")}
                        </button>
                        <div class="pageNo"><span>${pageNoInt}</span></div>
                        <button
                                name="page"
                                type="submit"
                                class="btn btn-default btn-sm pagingButtons"
                                value="${pageNoInt + 1}"
                                type="submit">
                            ${sessionScope.resourceService.findValueByKey("Next")}
                        </button>
                </form>
            </li>
        </ul>
    </div>
</div>

<hr>

<!-- Footer -->
<footer>
    <div class="container">
        <div class="row">
            <div class="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1">
                <ul class="list-inline text-center">
                    <li>
                        <a href="#">
                                <span class="fa-stack fa-lg">
                                    <i class="fa fa-circle fa-stack-2x"></i>
                                    <i class="fa fa-twitter fa-stack-1x fa-inverse"></i>
                                </span>
                        </a>
                    </li>
                    <li>
                        <a href="#">
                                <span class="fa-stack fa-lg">
                                    <i class="fa fa-circle fa-stack-2x"></i>
                                    <i class="fa fa-facebook fa-stack-1x fa-inverse"></i>
                                </span>
                        </a>
                    </li>
                    <li>
                        <a href="#">
                                <span class="fa-stack fa-lg">
                                    <i class="fa fa-circle fa-stack-2x"></i>
                                    <i class="fa fa-github fa-stack-1x fa-inverse"></i>
                                </span>
                        </a>
                    </li>
                </ul>
                <p class="copyright text-muted">Copyright &copy; Your Website 2016</p>
            </div>
        </div>
    </div>
</footer>

<!-- jQuery -->
<script src="${pageContext.request.contextPath}/theme/vendor/jquery/jquery.min.js" type="application/javascript"></script>

<!-- Bootstrap Core JavaScript -->
<script src="${pageContext.request.contextPath}/theme/vendor/bootstrap/js/bootstrap.min.js" type="application/javascript"></script>

<!-- Contact Form JavaScript -->
<script src="${pageContext.request.contextPath}/theme/js/jqBootstrapValidation.js" type="application/javascript"></script>
<script src="${pageContext.request.contextPath}/theme/js/contact_me.js" type="application/javascript"></script>

<!-- Theme JavaScript -->
<script src="${pageContext.request.contextPath}/theme/js/clean-blog.min.js" type="application/javascript"></script>

</body>

</html>
