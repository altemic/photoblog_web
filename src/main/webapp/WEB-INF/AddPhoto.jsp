<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page import="pl.altemic.photoblog.viewmodel.RecordViewModel" %>
<%--
  Created by IntelliJ IDEA.
  User: michal
  Date: 03.08.2016
  Time: 19:20
  To change this template use File | Settings | File Templates.
--%>
<!DOCTYPE html>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Add photo</title>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
    <meta name="viewport" content="width=device-width; initial-scale=1.0"/>
    <link href="${pageContext.request.contextPath}/theme/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/theme/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <script src="http://code.jquery.com/jquery-1.11.2.min.js"></script>
    <script src="http://code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
    <script src="${pageContext.request.contextPath}/theme/vendor/js/bootstrap.js"></script>
    <script src="${pageContext.request.contextPath}/js/jqueryrotate.js"></script>
    <script>

        var proportion;
        var loadFile = function(event) {
            var output = document.getElementById('imgView');
            output.src = URL.createObjectURL(event.target.files[0]);
            var initialHeight = $("#imgView").height();
            var initialWidth = $("#imgView").width();
            proportion = initialHeight /  initialWidth;
            scaleImg();
        };

        var scaleImg = function () {
            var frameWidth = $("#imgFrame").width();
            var desiredWidth = frameWidth;
            $("#imgView").css('width', desiredWidth);
        };

        var angle = 0;
        var rotateImg = function() {
            angle += 90;
            $("#imgView").rotate(angle);
            scaleImg();
            $("#angle").val(angle);
        };

    </script>
</head>
<body>

    <jsp:include page="fragments/header.jsp"></jsp:include>

    <div class="text-center">
        <c:choose>
            <c:when test="not empty ${requestScope.record!=null}">
                <h2 id="header">
                    ${sessionScope.resourceService.findValueByKey("Edit record")}
                </h2>
            </c:when>
            <c:otherwise>
                <h2 id="header">
                    ${sessionScope.resourceService.findValueByKey("Add record")}
                </h2>
            </c:otherwise>
        </c:choose>
    </div>

    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <form method="post" enctype="multipart/form-data">
                    <input type="hidden" id="angle" name="angle" value="0">
                    <div class="form-group">
                        <div class="form-group">
                            <label for="titleInput">
                                ${sessionScope.resourceService.findValueByKey("Title")}
                            </label>
                            <c:choose>
                                <c:when test="${requestScope.record!=null}">
                                    <input type="text" name="title" value="<c:out value="${requestScope.record.getTitle()}"></c:out>" id="titleInput" class="form-control">
                                </c:when>
                                <c:otherwise>
                                    <input type="text" name="title" placeholder="Title" id="titleInput" class="form-control">
                                </c:otherwise>
                            </c:choose>
                        </div>
                        <div class="form-group">
                            <label for="fileInput">
                                ${sessionScope.resourceService.findValueByKey("Image")}
                            </label>
                            <input type="file" name="image" placeholder="Photo" id="fileInput" onchange="loadFile(event)">
                        </div>
                        <div class="form-group">
                            <label for="descriptionInput">
                                ${sessionScope.resourceService.findValueByKey("Description")}
                            </label>
                            <c:choose>
                                <c:when test="${requestScope.record!=null}">
                                    <input type="text" name="description" value="<c:out value="${requestScope.record.getDescription()}"></c:out>" id="descriptionInput" class="form-control">
                                </c:when>
                                <c:otherwise>
                                    <input type="text" name="description" placeholder="Description" id="descriptionInput" class="form-control">
                                </c:otherwise>
                            </c:choose>
                        </div>
                    </div>
                    <c:choose>
                        <c:when test="${requestScope.record!=null}">
                            <input type="hidden" name="id" value="${requestScope.record.getId()}">
                            <input type="submit" value="${sessionScope.resourceService.findValueByKey("Update")}" formaction="${pageContext.request.contextPath}/records/update" class="btn btn-default">
                        </c:when>
                        <c:otherwise>
                            <input type="submit" value="${sessionScope.resourceService.findValueByKey("Add")}" formaction="${pageContext.request.contextPath}/records/add" class="btn btn-default">
                        </c:otherwise>
                    </c:choose>
                </form>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div style="height: 10px"></div>
                <button class="btn btn-default" onclick="rotateImg()">
                    ${sessionScope.resourceService.findValueByKey("Rotate")}
                </button>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12" id="imgFrame">
                <div id="spaceAboveImgView"></div>
                <c:choose>
                    <c:when test="${requestScope.record!=null}">
                        <img id="imgView" onload="scaleImg()" style="width: 700px" src="${pageContext.request.contextPath}/Pictures?id=${requestScope.record.getPicture().getId()}" />
                    </c:when>
                    <c:otherwise>
                        <img id="imgView"/>
                    </c:otherwise>
                </c:choose>
            </div>
        </div>
    </div>
</body>
</html>
