<%--
  Created by IntelliJ IDEA.
  User: michal
  Date: 18.10.2016
  Time: 20:00
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page import="pl.altemic.photoblog.viewmodel.ResourceViewModel" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
    <title>${sessionScope.resourceService.findValueByKey("Resource management")}</title>
    <meta name="viewport" content="width=device-width; initial-scale=1.0"/>
    <link href="${pageContext.request.contextPath}/theme/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/css/piesekfiona_custom.css" type="text/css" rel="stylesheet">
    <script src="${pageContext.request.contextPath}/theme/vendor/jquery/jquery.min.js" type="application/javascript"></script>
    <script src="${pageContext.request.contextPath}/theme/vendor/bootstrap/js/bootstrap.min.js" type="application/javascript"></script>
    <script src="${pageContext.request.contextPath}/theme/js/jqBootstrapValidation.js" type="application/javascript"></script>
    <style>
        .mediumColumn{
            width: 30%;
        }

        .shortColumn{
            width: 10%;
        }
    </style>

</head>
<body>
    <jsp:include page="fragments/header.jsp"></jsp:include>

    <div class="container">
        <div class="row">
            <div class="col-md-8">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4>${sessionScope.resourceService.findValueByKey("Control panel")}</h4>
                    </div>
                    <table class="table">
                        <thead>
                            <th class="mediumColumn">
                                <span>${sessionScope.resourceService.findValueByKey("Key")}</span>
                            </th>
                            <th class="mediumColumn">
                                <span>${sessionScope.resourceService.findValueByKey("Dict")}</span>
                            </th>
                            <th class="mediumColumn">
                                <span>${sessionScope.resourceService.findValueByKey("Value")}</span>
                            </th>
                            <th class="shortColumn">
                                <span>#</span>
                            </th>
                        </thead>
                        <tbody>
                            <c:forEach var="resource" items="${requestScope.resourcesList}">
                                <tr>
                                    <form action="${pageContext.request.contextPath}/resources/update?id=${resource.getId()}" method="post">
                                        <td>
                                            ${resource.getKey()}
                                            <input name="key" type="hidden" type="text" value="${resource.getKey()}">
                                        </td>
                                        <td>
                                            <input name="dict" type="text" value="${resource.getDict()}" style="width: 100%"/>
                                        </td>
                                        <td>
                                            <input name="value" type="text" value="${resource.getValue()}" style="width: 100%"/>
                                        </td>
                                        <td>
                                            <button class="btn btn-primary" style="width: 100%" type="submit">
                                                ${sessionScope.resourceService.findValueByKey("Update")}
                                            </button>
                                        </td>
                                    </form>
                                </tr>
                            </c:forEach>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="col-md-3 col-lg-offset-1">
                <table>
                    <tr>
                        <td>
                            <div class="btn-group" role="group">
                                <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    ${requestScope.dictionaryList.get(0)} <span class="caret"></span>
                                </button>
                                <ul class="dropdown-menu">
                                    <c:forEach var="dict" items="${requestScope.dictionaryList}">
                                        <li><a href="${pageContext.request.contextPath}/resources?lang=${dict}">${dict}</a></li>
                                    </c:forEach>
                                </ul>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <table>
                            <tr>
                                <td>${sessionScope.resourceService.findValueByKey("Add language support")}</td>
                            </tr>
                            <tr>
                                <td>
                                    <form method="post" action="${pageContext.request.contextPath}/resources/new">
                                        <input type="text" name="newLanguage">
                                        <button type="submit">${sessionScope.resourceService.findValueByKey("Add")}</button>
                                    </form>
                                </td>
                            </tr>
                        </table>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</body>
</html>
