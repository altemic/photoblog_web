<%--
  Created by IntelliJ IDEA.
  User: michal
  Date: 03.09.2016
  Time: 14:13
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<nav class="navbar navbar-default">
    <div class="container">
        <div class="collapse navbar-collapse navbar-fixed-top">
            <ul class="nav navbar-nav navbar-right">
                <li><a href="${pageContext.request.contextPath}/">${sessionScope.resourceService.findValueByKey("Main page")}</a></li>
                <li><a href="${pageContext.request.contextPath}/records/add">${sessionScope.resourceService.findValueByKey("Add record")}</a></li>
                <li><a href="${pageContext.request.contextPath}/controlpanel">${sessionScope.resourceService.findValueByKey("Control panel")}</a></li>
            </ul>
        </div>
    </div>
</nav>
