<%--
  Created by IntelliJ IDEA.
  User: michal
  Date: 16.10.2016
  Time: 10:40
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>Control panel</title>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
    <meta name="viewport" content="width=device-width; initial-scale=1.0"/>
    <link href="${pageContext.request.contextPath}/theme/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/css/piesekfiona_custom.css" type="text/css" rel="stylesheet">

    <style>
        .mediumColumn{
            width: 40%;
        }

        .shortColumn{
            width: 100%;
        }
    </style>

</head>
<body>
    <jsp:include page="fragments/header.jsp"></jsp:include>

    <div class="container">
        <div class="row">
            <div class="col-md-8">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4>${sessionScope.resourceService.findValueByKey("Control panel")}</h4>
                    </div>
                    <table class="table">
                        <thead>
                            <th class="mediumColumn">
                                <span>${sessionScope.resourceService.findValueByKey("Field")}</span>
                            </th>
                            <th class="mediumColumn">
                                <span>${sessionScope.resourceService.findValueByKey("Value")}</span>
                            </th>
                            <th class="shortColumn">
                                <span>#</span>
                            </th>
                        </thead>
                        <tbody>
                            <c:forEach var="property" items="${sessionScope.Config.getAll()}">
                                <tr>
                                    <form action="${pageContext.request.contextPath}/controlpanel/update?name=${property.key}" method="post">
                                        <td>
                                            ${property.key}
                                        </td>
                                        <td>
                                            <input name="value" type="text" value="${property.value}" style="width: 100%"/>
                                        </td>
                                        <td>
                                            <button class="btn btn-primary" style="width: 100%" type="submit">
                                                ${sessionScope.resourceService.findValueByKey("Update")}
                                            </button>
                                        </td>
                                    </form>
                                </tr>
                            </c:forEach>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="col-md-3 col-lg-offset-1">
                <table>
                    <tr>
                        <td>
                            <form method="get" action="${pageContext.request.contextPath}/resources">
                                <button type="submit" class="btn btn-default btn-sm pagingButtons">
                                    ${sessionScope.resourceService.findValueByKey("Resources")}
                                </button>
                            </form>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <form method="get" action="${pageContext.request.contextPath}/users">
                                <button type="submit" class="btn btn-default btn-sm pagingButtons">
                                    ${sessionScope.resourceService.findValueByKey("Users")}
                                </button>
                            </form>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>

</body>
</html>
